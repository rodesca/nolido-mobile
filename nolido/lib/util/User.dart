import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';


class User {



  final int? id;
  final String userLogin;
  final int fullName;
  final String idContryName;
  final String? deviseContryName;


  User(
      { this.id,
        required this.userLogin,
        required this.fullName,
        required this.idContryName,
        required this.deviseContryName,

      });

  User.fromMap(Map<String, dynamic> res)
      : id = res["id"],
        userLogin = res["userLogin"],
        fullName = res["fullName"],
        idContryName = res["idContryName"],
        deviseContryName = res["deviseContryName"];


  Map<String, Object?> toMap() {
    return {'id':id,'userLogin': userLogin, 'fullName': fullName, 'idContryName': idContryName, 'deviseContryName': deviseContryName,};
  }
}


class DatabaseHandler {
  Future<Database> initializeDB() async {
    String path = await getDatabasesPath();
    return openDatabase(
      join(path, 'example.db'),
      onCreate: (database, version) async {
        await database.execute(
          "CREATE TABLE users(id INTEGER PRIMARY KEY AUTOINCREMENT,"
              " userLogin TEXT NOT NULL,"
              "fullName INTEGER NOT NULL,"
              " idContryName TEXT NOT NULL,"
              " deviseContryName TEXT)",
        );
      },
      version: 1,
    );
  }
}

