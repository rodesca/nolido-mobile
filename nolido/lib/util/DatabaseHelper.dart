import 'package:nolido/util/User.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';


class DatabaseHelper {
  Future<Database> initializeDB() async {
    String path = await getDatabasesPath();
    return openDatabase(
      join(path, 'example.db'),
      onCreate: (database, version) async {
        await database.execute(
          "CREATE TABLE users(id INTEGER PRIMARY KEY AUTOINCREMENT,"
              " userLogin TEXT NOT NULL,"
              "fullName INTEGER NOT NULL,"
              " idContryName TEXT NOT NULL,"
              " deviseContryName TEXT)",
        );
      },
      version: 1,
    );
  }
  Future<int> insertUser(List<User> users) async {
    int result = 0;
    final Database db = await initializeDB();
    for(var user in users){
      result = await db.insert('users', user.toMap());
    }
    return result;
  }



  Future<int> addUsers(userLogin, fullName, idContryName, deviseContryName) async {
    User firstUser = User(userLogin: userLogin, fullName: fullName, idContryName: idContryName, deviseContryName: deviseContryName);
    List<User> listOfUsers = [firstUser];
    return await insertUser(listOfUsers);

  }

}

