import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nolido/ui/HomeUI.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]).then((_) {
    runApp(new MyApp());
  });
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Nolido',
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      home: const HomeUI(title : "Nolido"),
    );
  }
}
