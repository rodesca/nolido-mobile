import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:nolido/ui/Home.dart';

class HomeUI extends StatefulWidget {
  const HomeUI({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<HomeUI> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<HomeUI> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Stack(
              children: [
                imageScreen(),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.only(top: 250, left: 20),
                      child: Text(
                        "Bienvenue",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 25,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 5, left: 20),
                      child: Text(
                        "Sur Nolido",
                        style: TextStyle(color: Colors.white70, fontSize: 15),
                      ),
                    ),
                    Row(
                      children: [
                        Container(
                          padding: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height / 4.3,
                              left: 20),
                          child: Row(
                            children: [
                              ElevatedButton(
                                onPressed: () {
                                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => Home()));
                                },
                                child: Text(
                                  "Commençons ",
                                  style: TextStyle(
                                      color: Colors.white70,
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              Icon(
                                Icons.arrow_forward,
                                color: Colors.white70,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget imageScreen() {
    return Container(
      color: Colors.orange,
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Image.asset(
        "images/Groupe.png",
        fit: BoxFit.cover,
      ),
    );
  }
}
