import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:intl/intl.dart';

class DetailTransfert extends StatefulWidget {
  String transfertDate;
  String transfertMethod;
  String countrySourceCd;
  double sendingAmount;
  double rate;
  double receiveAmount;
  double feeAmount;
  double dueAmount;
  String sendCurrency;
  String receiveCurrency;
  String receivePhone;
  String operator;
  String targetCountry;
  String receiver;

  DetailTransfert(
      this.transfertDate,
      this.transfertMethod,
      this.countrySourceCd,
      this.sendingAmount,
      this.rate,
      this.receiveAmount,
      this.feeAmount,
      this.dueAmount,
      this.sendCurrency,
      this.receiveCurrency,
      this.receivePhone,
      this.operator,
      this.targetCountry,
      this.receiver,
      {Key? key})
      : super(key: key);

  @override
  _DetailTransfertState createState() => _DetailTransfertState();
}
final formatCurrency = new NumberFormat("#,##0.00");
class _DetailTransfertState extends State<DetailTransfert> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Column(
        children: [
          Container(
            color: Colors.orangeAccent,
            height: MediaQuery.of(context).size.height / 2,
            width: MediaQuery.of(context).size.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    padding: EdgeInsets.fromLTRB(5, 35, 0, 0),
                    child: Icon(
                      Icons.arrow_back_ios_outlined,
                      color: Colors.white,
                      size: 25,
                    ),
                  ),
                ),

                Container(
                  padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
                  child: Center(
                    child: Text(
                        formatCurrency.format(widget.sendingAmount) +
                          ' ' +
                          widget.sendCurrency,
                      style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Center(
                  child: Container(
                    height: 50,
                    decoration: BoxDecoration(
                      color: Colors.grey,
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(10),
                          topLeft: Radius.circular(10)),
                    ),
                    width: MediaQuery.of(context).size.width / 1.07,
                    child: Container(
                      child: Center(
                        child: Text(
                          widget.receiver,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 17,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Center(
                  child: Container(
                    height: 100,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(10),
                          bottomRight: Radius.circular(10)),
                    ),
                    width: MediaQuery.of(context).size.width / 1.07,
                    child: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Reçu : " +
                          formatCurrency.format(widget.receiveAmount) +
                                    " (" +
                                    widget.receiveCurrency +
                                    ')',
                                style: TextStyle(fontSize: 16),
                              ),
                              Text(
                                "Taux : " + widget.rate.toString(),
                                style: TextStyle(fontSize: 16),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(10, 10, 10, 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Total : " +
                                    formatCurrency.format(widget.dueAmount) +
                                    ' (' +
                                    widget.sendCurrency +
                                    ')',
                                style: TextStyle(fontSize: 16),
                              ),
                              Text(
                                "Frais : " + widget.feeAmount.toString(),
                                style: TextStyle(fontSize: 16),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Center(
            child: Container(
              height: 50,
              decoration: BoxDecoration(
                color: Colors.black45,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(10),
                    topLeft: Radius.circular(10)),
              ),
              width: MediaQuery.of(context).size.width / 1.07,
              child: Center(
                child: Text(
                 widget.receivePhone.toString(),
                  style: TextStyle(fontSize: 17, color: Colors.white),
                ),
              ),
            ),
          ),
          Center(
            child: Container(
              height: 100,
              decoration: BoxDecoration(
                color: Colors.orangeAccent,
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10),
                    bottomRight: Radius.circular(10)),
              ),
              width: MediaQuery.of(context).size.width / 1.07,
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                        child: Text(
                          "Exp : " + widget.countrySourceCd.toString(),
                          style: TextStyle(fontSize: 16, color: Colors.white),
                        ),
                      ),

                      Container(
                        padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                        child: Text(
                          "" + widget.transfertMethod.toString() == "001" ? "CarteVisa" :  widget.transfertMethod.toString() == "002" ? ""+"MasterCard" : "003",
                          style: TextStyle(fontSize: 16, color: Colors.white),
                        ),
                      ),


                    ],
                  ),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        padding: EdgeInsets.fromLTRB(10, 30, 10, 0),
                        child: Text(
                          "Dest : " + widget.targetCountry.toString(),
                          style: TextStyle(fontSize: 16, color: Colors.white),
                        ),
                      ),

                      Container(
                        padding: EdgeInsets.fromLTRB(10, 30, 10, 0),
                        child: Text(
                          "" + widget.operator.toString(),
                          style: TextStyle(fontSize: 16, color: Colors.white),
                        ),
                      ),


                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
