import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:nolido/widget/Composant.dart';
import 'package:toast/toast.dart';


class LoadingError extends StatefulWidget {
  const LoadingError({Key? key}) : super(key: key);

  @override
  _LoadingErrorState createState() => _LoadingErrorState();
}

class _LoadingErrorState extends State<LoadingError> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Timer(Duration(seconds: 2), callConnexionErreur);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: loading(),
          ),
        ],
      ),
    );
  }


  callback(){
    Navigator.pop(context);
    //Toast Message
    ToastContext().init(context);
    Composant().showToast(
        "true", "Votre compte nolido à été créer",
        duration: Toast.lengthLong, gravity: Toast.bottom);


  }

  callConnexionErreur(){
    //Toast Message
    Navigator.pop(context);
  }

  Widget loading(){

    return SpinKitSpinningLines(color: Colors.orangeAccent, size: 100, duration: Duration(seconds: 1),lineWidth: 5,
    );
  }
}