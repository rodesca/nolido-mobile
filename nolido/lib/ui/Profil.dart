import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:nolido/ui/Compte.dart';
import 'package:nolido/ui/MdpChanger.dart';
import 'package:nolido/ui/Notifications.dart';
import 'package:nolido/ui/VerifierCompte.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Profil extends StatefulWidget {
  String fullName = "";
  String countryIndicative = "";
  String account = "";
  String phone = "";
  String password = "";

   Profil({Key? key}) : super(key: key){
    getSharedPrefs();
  }

  Future<Null> getSharedPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    this.fullName = prefs.getString("fullName")!;
    this.countryIndicative = prefs.getString("countryIndicative")!;
    this.account = prefs.getString("account")!;
    this.phone = prefs.getString("phone")!;
    this.password = prefs.getString("password")!;
  }

  @override
  _ProfilState createState() => _ProfilState();
}

class _ProfilState extends State<Profil> {

  Future<Null> getSharedPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    widget.fullName = prefs.getString("fullName")!;
    widget.countryIndicative = prefs.getString("countryIndicative")!;
    widget.account = prefs.getString("account")!;
    widget.phone = prefs.getString("phone")!;
    widget.password = prefs.getString("password")!;
  }

  bool _loading = true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {

      showProgress();
      Timer(Duration(seconds: 2), hideProgress);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              children: [
                Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.fromLTRB(20, 46, 0, 0),
                  child: Column(
                    children: [
                      Text(
                        "Profil",
                        style: TextStyle(
                            fontSize: 25,
                            color: Colors.black54,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
                Row(
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(25, 18, 0, 0),
                      child: CircleAvatar(
                        radius: 30,
                        child: Image.asset("images/picture.png"),
                      ),
                    ),
                    SizedBox(
                      width: 12,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: EdgeInsets.fromLTRB(0, 15, 0, 0),
                          child: Text(
                            widget.fullName.toString().substring(0, 10) + '...',
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                fontSize: 12,
                                color: Colors.black54,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(0, 8, 0, 0),
                          child: Text(
                            "Modifier",
                            style: TextStyle(fontSize: 13, color: Colors.black54),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(20, 50, 20, 20),
                  width: MediaQuery.of(context).size.width,
                  height: 20,
                  color: Colors.grey[300],
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => VerifierCompte()));
                    },
                    child: Container(
                      padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                      child: Center(
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                MenuOption("Compte", widget.fullName, widget.countryIndicative, widget.account, widget.phone, widget.password),
                MenuOption("Changer mot de passe","","","", "", ""),
                MenuOption("Support","","","", "", ""),
              ],
            ),
          ),
          new Offstage(
            offstage: _loading,
            child: _progressWidget(),
          ),
        ],
      ),
    );
  }

  Widget _progressWidget() {
    return new Center(
      child: new Stack(
        children: <Widget>[
          new Container(
            color: Colors.black45,
          ),
          Center(
            child: new SpinKitSpinningLines(
              color: Colors.orangeAccent,
              size: 100,
              lineWidth: 5,
            ),
          )
        ],
      ),
    );
  }

  //Navigator.of(context)
  //  .push(MaterialPageRoute(builder: (context) => LoadingStyle()));

  void showProgress() {
    if (mounted) {
      setState(() {
        print(">>>>>> showProgress >>> _loading : ${_loading}");

        _loading = false;

        print(">>>>>> showProgress >>> _loading : ${_loading}");
      });
    }
  }

  void hideProgress() {
    if (mounted) {
      setState(() {
        print(">>>>>> hideProgress >>> ");
        _loading = true;
      });
    }
  }

  Widget MenuOption(String name, String full, String countryInd, String UserLogin, String tel, String password) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.fromLTRB(8, 0, 10, 0),
          height: 56,
          width: MediaQuery.of(context).size.width,
          color: Colors.white,
          child: ElevatedButton(
            onPressed: () {
              if (name == "Compte") {

                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) => Compte(full, countryInd, UserLogin, tel, password)));
              } else {
                if (name == "Notifications") {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => Notifications()));
                }else{
                  if (name == "Changer mot de passe") {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => MdpChanger()));
                  }
                }
              }
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "$name",
                  style: TextStyle(fontSize: 15, color: Colors.black54),
                ),
                name == "Notifications"
                    ? Icon(
                        Icons.add_alert_outlined,
                        color: Colors.black54,
                        size: 20,
                      )
                    : Icon(Icons.arrow_forward_ios_rounded,
                        color: Colors.black54, size: 20),
              ],
            ),
          ),
        ),
        Container(
          width: MediaQuery.of(context).size.width / 1.1,
          child: Divider(
            color: Colors.black54,
          ),
        ),
      ],
    );
  }
}
