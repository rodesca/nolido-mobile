import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:nolido/ui/ConfirmeTrans.dart';
import 'package:nolido/ui/registration/NolidoConstants.dart';
import 'package:nolido/widget/Composant.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class EffectuerTrans extends StatefulWidget {
  String? userCurrency;
  String? userFullName;
  String? userContryIndicative;
  String? userCountryID;
  String? userAccount;
  String? userPassword;

  EffectuerTrans({Key? key}) : super(key: key) {
    getSharedPrefs();
  }

  @override
  _EffectuerTransState createState() => _EffectuerTransState();

  Future<Null> getSharedPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    this.userCurrency = prefs.getString("currency");
    this.userFullName = prefs.getString("fullName");
    this.userContryIndicative = prefs.getString("countryIndicative");
    this.userCountryID = prefs.getString("countryID");
    this.userAccount = prefs.getString("account");
    this.userPassword = prefs.getString("password");
  }
}

class _EffectuerTransState extends State<EffectuerTrans> {
  String indexMVGSM = "";
  var VarTel;
  var inPays;
  var VarMdp;
  String Mode = "";
  String ModeF = "MTN";
  String NumberPhone = "";
  bool _loading = true;
  TextEditingController controllerCodepin = new TextEditingController();
  TextEditingController textNumberPhone = new TextEditingController();
  TextEditingController txtMontantEnvoi = new TextEditingController();
  TextEditingController txtFullName = new TextEditingController();

  Future<Null> getSharedPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    widget.userCurrency = prefs.getString("currency");
    widget.userFullName = prefs.getString("fullName");
    widget.userContryIndicative = prefs.getString("countryIndicative");
    widget.userCountryID = prefs.getString("countryID");
    widget.userAccount = prefs.getString("account");
    widget.userPassword = prefs.getString("password");
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      widget.userCurrency = widget.userCurrency;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                Row(
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(0, 35, 0, 0),
                      child: IconButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          icon: Icon(
                            Icons.arrow_back_ios_outlined,
                            color: Colors.black54,
                          )),
                    )
                  ],
                ),
                Column(
                  children: [
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                      child: Column(
                        children: [
                          Text(
                            "Effectuer ",
                            style: TextStyle(
                                fontSize: 23,
                                color: Colors.black54,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                      child: Column(
                        children: [
                          Text(
                            "Votre transaction ",
                            style: TextStyle(
                                fontSize: 23,
                                color: Colors.black54,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.fromLTRB(20, 0, 10, 0),
                      child: Column(
                        children: [
                          Text(
                            "Pour pouvez changer le numéro expediteur dans le menu profil.",
                            style: TextStyle(
                                fontSize: 15,
                                color: Colors.black54,
                                fontWeight: FontWeight.normal),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    PropetiesCompteFinance("Mode de financement", 'Finance'),
                    SizedBox(
                      height: 10,
                    ),
                    PropetiesCompte("Mode de reception", 'Reception'),
                    SizedBox(
                      height: 15,
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
                      child: TextField(
                        autocorrect: true,
                        controller: txtFullName,
                        keyboardType: TextInputType.text,
                        obscureText: false,
                        decoration: InputDecoration(
                          labelText: 'Nom(s) & Prénom(s) destinataire',
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                      child: Column(
                        children: [
                          phoneTF2("Numéro de télephone destinataire"),
                        ],
                      ),
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                      child: Column(
                        children: [
                          numberPaiement("", "Montant d'envoi"),
                        ],
                      ),
                    ),
                  ],
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 85,
                  padding: EdgeInsets.fromLTRB(20, 30, 20, 10),
                  child: ElevatedButton(
                    onPressed: () {
                      var num = VarTel;
                      print("ModeF " + ModeF);
                      print("Mode " + Mode);
                      //  print("inPays "+inPays);
                      //        print("num "+num);
                      if (ModeF == "" ||
                          Mode == "" ||
                          VarTel == "" ||
                          txtFullName.text == "" ||
                          txtMontantEnvoi.text == "" ||
                          VarTel == "") {
                        //Toast Message
                        ToastContext().init(context);
                        Composant().showToast(
                            "false", "Veuiller remplir vos champs de saisie",
                            duration: Toast.lengthLong, gravity: Toast.bottom);
                      } else {
                        num = "";
                        num = inPays + VarTel;
                        print(
                            'widget.userPassword.toString() $widget.userPassword.toString()');
                        if (ModeF == "MasterCard") {
                          indexMVGSM = "";
                          indexMVGSM = "001";
                          connexionToken(
                            txtFullName.text,
                            widget.userAccount.toString(),
                            widget.userPassword.toString(),
                            inPays,
                            widget.userCountryID.toString(),
                            indexMVGSM,
                            double.parse(txtMontantEnvoi.text),
                            "",
                          );
                        } else {
                          if (ModeF == "CarteVisa") {
                            indexMVGSM = "";
                            indexMVGSM = "VISA";
                            connexionToken(
                              txtFullName.text,
                              widget.userAccount.toString(),
                              widget.userPassword.toString(),
                              inPays,
                              widget.userCountryID.toString(),
                              indexMVGSM,
                              double.parse(txtMontantEnvoi.text),
                              "",
                            );
                          } else {
                            indexMVGSM = "";
                            indexMVGSM = "GSM";
                            openCodeGSM(context);
                          }
                        }
                      }
                    },
                    child: Text(
                      "Suivant",
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                  ),
                ),
              ],
            ),
          ),
          new Offstage(
            offstage: _loading,
            child: _progressWidget(),
          ),
        ],
      ),
    );
  }

  Widget PropetiesCompteFinance(String propeties, String type) {
    return GestureDetector(
      onTap: () {},
      child: Card(
        color: Colors.white,
        elevation: 10,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        child: Container(
          margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
          width: MediaQuery.of(context).size.width / 1.1,
          height: 65,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
          ),
          child: ElevatedButton(
            onPressed: () {
              if (type == "Finance") {
                print("Modal finance");
                ModalFinance(context);
              } else {
                print("Modal reception");
                ModalRecep(context);
              }
            },
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(10, 20, 10, 0),
                      child: Text(
                        '$propeties',
                        style: TextStyle(
                          fontSize: 15,
                          color: Colors.black54,
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(10, 17, 0, 0),
                      child: Row(
                        children: [
                          propeties == "Mode de "
                              ? Icon(
                                  Icons.mode_comment_outlined,
                                  color: Colors.black54,
                                  size: 22,
                                )
                              : ModeF == "MTN"
                                  ? Image.asset(
                                      "images/mtn.jpg",
                                      height: 35,
                                      width: 35,
                                    )
                                  : ModeF == "ORANGE"
                                      ? Image.asset(
                                          "images/orange2.png",
                                          height: 35,
                                          width: 35,
                                        )
                                      : ModeF == "AIRTEL"
                                          ? Image.asset(
                                              "images/airtel.png",
                                              height: 35,
                                              width: 35,
                                            )
                                          : ModeF == "FREE"
                                              ? Image.asset(
                                                  "images/free.png",
                                                  height: 35,
                                                  width: 35,
                                                )
                                              : ModeF == "MasterCard"
                                                  ? Image.asset(
                                                      "images/mastercard.png",
                                                      height: 40,
                                                      width: 40,
                                                    )
                                                  : ModeF == "CarteVisa"
                                                      ? Image.asset(
                                                          "images/visa1.png",
                                                          height: 35,
                                                          width: 35,
                                                        )
                                                      : Image.asset(
                                                          "images/mtn.jpg",
                                                          height: 35,
                                                          width: 35,
                                                        ),
                          Icon(
                            Icons.arrow_forward_ios_rounded,
                            color: Colors.black54,
                            size: 22,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget PropetiesCompte(String propeties, String type) {
    return GestureDetector(
      onTap: () {},
      child: Card(
        color: Colors.white,
        elevation: 10,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        child: Container(
          margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
          width: MediaQuery.of(context).size.width / 1.1,
          height: 65,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
          ),
          child: ElevatedButton(
            onPressed: () {
              if (type == "Finance") {
                print("Modal finance");
                ModalFinance(context);
              } else {
                print("Modal reception");
                ModalRecep(context);
              }
            },
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(10, 20, 10, 0),
                      child: Text(
                        '$propeties',
                        style: TextStyle(
                          fontSize: 15,
                          color: Colors.black54,
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(10, 17, 0, 0),
                      child: Row(
                        children: [
                          propeties == "Mode de financement"
                              ? Icon(
                                  Icons.mode_comment_outlined,
                                  color: Colors.black54,
                                  size: 22,
                                )
                              : Mode == "MTN"
                                  ? Image.asset(
                                      "images/mtn.jpg",
                                      height: 35,
                                      width: 35,
                                    )
                                  : Mode == "ORANGE"
                                      ? Image.asset(
                                          "images/orange2.png",
                                          height: 35,
                                          width: 35,
                                        )
                                      : Mode == "AIRTEL"
                                          ? Image.asset(
                                              "images/airtel.png",
                                              height: 35,
                                              width: 35,
                                            )
                                          : Mode == "FREE"
                                              ? Image.asset(
                                                  "images/free.png",
                                                  height: 35,
                                                  width: 35,
                                                )
                                              : Icon(
                                                  Icons.mode_comment_outlined,
                                                  color: Colors.black54,
                                                  size: 22,
                                                ),
                          Icon(
                            Icons.arrow_forward_ios_rounded,
                            color: Colors.black54,
                            size: 22,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void ModalRecep(context) {
    showModalBottomSheet(
        backgroundColor: Colors.grey[800],
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(30), topLeft: Radius.circular(30)),
        ),
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return Container(
            height: MediaQuery.of(context).size.height / 1.7,
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Container(
                    child: Column(
                      children: [
                        // Composant()
                        //   .texteTF("", AppConst.last_name_destinataire),
                        SizedBox(
                          height: 15,
                        ),
                        // Composant().texteTfReadOnly(
                        //   "", AppConst.first_name_destinataire),
                      ],
                    ),
                  ),

                  Row(
                    children: [
                      Container(
                        padding: EdgeInsets.fromLTRB(20, 10, 0, 10),
                        child: Text(
                          "Mode de reception",
                          style: TextStyle(color: Colors.white, fontSize: 18),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ],
                  ),

                  SizedBox(
                    height: 15,
                  ),
                  TextButton(
                    onPressed: () {
                      setState(() {
                        Mode = "MTN";
                      });
                      print(Mode);
                      Navigator.pop(context);
                    },
                    child: Container(
                      margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      padding: EdgeInsets.fromLTRB(25, 5, 0, 0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Image.asset(
                            "images/mtn.jpg",
                            height: 50,
                            width: 50,
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(10, 15, 0, 0),
                            child: Text(
                              "MTN - Money",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 16),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  SizedBox(
                    height: 15,
                  ),

                  TextButton(
                    onPressed: () {
                      setState(() {
                        Mode = "ORANGE";
                      });
                      print(Mode);
                      Navigator.pop(context);
                    },
                    child: Container(
                      margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      padding: EdgeInsets.fromLTRB(25, 5, 0, 0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Image.asset(
                            "images/orange2.png",
                            height: 50,
                            width: 50,
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(10, 15, 0, 0),
                            child: Text(
                              "ORANGE - Money",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 16),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  SizedBox(
                    height: 15,
                  ),

                  TextButton(
                    onPressed: () {
                      setState(() {
                        Mode = "AIRTEL";
                      });
                      print(Mode);
                      Navigator.pop(context);
                    },
                    child: Container(
                      margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      padding: EdgeInsets.fromLTRB(25, 5, 0, 0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Image.asset(
                            "images/airtel.png",
                            height: 48,
                            width: 48,
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(10, 15, 0, 0),
                            child: Text(
                              "AIRTEL - Money",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 16),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),

                  TextButton(
                    onPressed: () {
                      setState(() {
                        Mode = "FREE";
                      });
                      print(Mode);
                      Navigator.pop(context);
                    },
                    child: Container(
                      margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      padding: EdgeInsets.fromLTRB(25, 5, 0, 0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Image.asset(
                            "images/free.png",
                            height: 50,
                            width: 50,
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(10, 15, 0, 0),
                            child: Text(
                              "FREE - Money",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 16),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),


                  //  BtnConfirmationTrans(context),
                ],
              ),
            ),
          );
        });
  }

  void ModalFinance(context) {
    showModalBottomSheet(
        backgroundColor: Colors.grey[800],
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(30), topLeft: Radius.circular(30)),
        ),
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return Container(
            height: MediaQuery.of(context).size.height / 1.3,
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Container(
                    child: Column(
                      children: [
                        // Composant()
                        //   .texteTF("", AppConst.last_name_destinataire),
                        SizedBox(
                          height: 15,
                        ),
                        // Composant().texteTfReadOnly(
                        //   "", AppConst.first_name_destinataire),
                      ],
                    ),
                  ),

                  Row(
                    children: [
                      Container(
                        padding: EdgeInsets.fromLTRB(20, 10, 0, 10),
                        child: Text(
                          "Mode de financement",
                          style: TextStyle(color: Colors.white, fontSize: 18),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ],
                  ),

                  SizedBox(
                    height: 15,
                  ),
                  TextButton(
                    onPressed: () {
                      setState(() {
                        ModeF = "MTN";
                      });
                      print(ModeF);
                      Navigator.pop(context);
                    },
                    child: Container(
                      margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      padding: EdgeInsets.fromLTRB(25, 5, 0, 0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Image.asset(
                            "images/mtn.jpg",
                            height: 50,
                            width: 50,
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(10, 15, 0, 0),
                            child: Text(
                              "MTN - Money",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 16),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  SizedBox(
                    height: 15,
                  ),

                  TextButton(
                    onPressed: () {
                      setState(() {
                        ModeF = "ORANGE";
                      });
                      print(ModeF);
                      Navigator.pop(context);
                    },
                    child: Container(
                      margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      padding: EdgeInsets.fromLTRB(25, 5, 0, 0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Image.asset(
                            "images/orange2.png",
                            height: 50,
                            width: 50,
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(10, 15, 0, 0),
                            child: Text(
                              "ORANGE - Money",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 16),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  SizedBox(
                    height: 15,
                  ),

                  TextButton(
                    onPressed: () {
                      setState(() {
                        ModeF = "AIRTEL";
                      });
                      print(ModeF);
                      Navigator.pop(context);
                    },
                    child: Container(
                      margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      padding: EdgeInsets.fromLTRB(25, 5, 0, 0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Image.asset(
                            "images/airtel.png",
                            height: 48,
                            width: 48,
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(10, 15, 0, 0),
                            child: Text(
                              "AIRTEL - Money",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 16),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),

                  TextButton(
                    onPressed: () {
                      setState(() {
                        ModeF = "FREE";
                      });
                      print(ModeF);
                      Navigator.pop(context);
                    },
                    child: Container(
                      margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      padding: EdgeInsets.fromLTRB(25, 5, 0, 0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Image.asset(
                            "images/free.png",
                            height: 50,
                            width: 50,
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(10, 15, 0, 0),
                            child: Text(
                              "FREE - Money",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 16),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  SizedBox(
                    height: 15,
                  ),

                  TextButton(
                    onPressed: () {
                      setState(() {
                        ModeF = "MasterCard";
                      });
                      print(ModeF);
                      Navigator.pop(context);
                    },
                    child: Container(
                      margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      padding: EdgeInsets.fromLTRB(25, 5, 0, 0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Image.asset(
                            "images/mastercard.png",
                            height: 50,
                            width: 50,
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(10, 15, 0, 0),
                            child: Text(
                              "MasterCard",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 16),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  SizedBox(
                    height: 15,
                  ),

                  TextButton(
                    onPressed: () {
                      setState(() {
                        ModeF = "CarteVisa";
                      });
                      print(ModeF);
                      Navigator.pop(context);
                    },
                    child: Container(
                      margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      padding: EdgeInsets.fromLTRB(25, 5, 0, 0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Image.asset(
                            "images/visa1.png",
                            height: 50,
                            width: 50,
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(10, 15, 0, 0),
                            child: Text(
                              "CarteVisa",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 16),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  //  BtnConfirmationTrans(context),
                ],
              ),
            ),
          );
        });
  }

  void change() {
    IntlPhoneField(
        initialValue: "",
        onChanged: (phone) {
          VarTel = "";
          print(inPays);
        });
  }

  Widget phoneTF2(String label) => IntlPhoneField(
        controller: textNumberPhone,
        disableLengthCheck: true,
        initialValue: "",
        onCountryChanged: (value) {
          return change();
        },
        decoration: InputDecoration(
          focusColor: Colors.orange,
          labelText: label,
        ),
        initialCountryCode: 'SN',
        invalidNumberMessage: "Numéro invalide",
        onChanged: (phone) {
          setState(() {
            VarTel = phone.number;
            inPays = phone.countryCode;
          });
        },
      );

  Widget numberPaiement(Object controler, String labelText) => TextField(
        // inputFormatters: [CurrencyTextInputFormatter()],
        decoration: InputDecoration(
          prefixText: "${widget.userCurrency}   ",
          suffixIcon: Image(
            image: ModeF == "MTN"
                ? AssetImage("images/mtn.jpg")
                : ModeF == "AIRTEL"
                    ? AssetImage("images/airtel.png")
                    : ModeF == "ORANGE"
                        ? AssetImage("images/orange2.png")
                        : ModeF == "FREE"
                            ? AssetImage("images/free.png")
                            : ModeF == "MasterCard"
                                ? AssetImage("images/mastercard.png")
                                : ModeF == "CarteVisa"
                                    ? AssetImage("images/visa1.png")
                                    : AssetImage("images/mtn.jpg"),
            width: 20,
            height: 20,
            fit: BoxFit.contain,
          ),
          //  border: OutlineInputBorder(),
          focusColor: Colors.orange,
          labelText: labelText,
          hintText: labelText,
        ),
        controller: txtMontantEnvoi,
        textInputAction: TextInputAction.done,
        keyboardType: TextInputType.number,
      );

  Widget _progressWidget() {
    return new Center(
      child: new Stack(
        children: <Widget>[
          new Container(
            color: Colors.black45,
          ),
          Center(
            child: new SpinKitSpinningLines(
              color: Colors.orangeAccent,
              size: 100,
              lineWidth: 5,
            ),
          )
        ],
      ),
    );
  }

  //Navigator.of(context)
  //  .push(MaterialPageRoute(builder: (context) => LoadingStyle()));

  void showProgress() {
    if (mounted) {
      setState(() {
        print(">>>>>> showProgress >>> _loading : ${_loading}");

        _loading = false;

        print(">>>>>> showProgress >>> _loading : ${_loading}");
      });
    }
  }

  void hideProgress() {
    if (mounted) {
      setState(() {
        print(">>>>>> hideProgress >>> ");
        _loading = true;
      });
    }
  }

  //Fonction permettant de se connecter a l'application
  Future<dynamic> connexionToken(
    String receiver,
    String userLogin,
    String password,
    String targetCountryIndicative,
    String countrySourceCd,
    String transfertMethod,
    double sendingAmount,
    String controllerCodepin,
  ) async {
    showProgress();
    String url = NolidoConstants.KEYCLOACK_URL;
    // 'https://www.qibisoft.com:8443/auth/realms/nolido/protocol/openid-connect/token';

    print(receiver);
    print(userLogin);
    print(password);
    print(targetCountryIndicative);
    print(countrySourceCd);
    print(transfertMethod);
    print(sendingAmount);
    print(controllerCodepin);

    Map<String, String> headers = {
      'content-type': 'application/x-www-form-urlencoded',
      'cache-control': 'no-cache',
    };

    Map<String, String> data = {
      'username': userLogin,
      'password': password,
      'grant_type': 'password',
      'client_id': 'nolido-mobile-application'
    };

    Uri uri = Uri.parse(url);
    http.Response response = await http.post(uri, headers: headers, body: data);
    // print(response.statusCode);
    hideProgress();
    if (response.statusCode == 200) {
      showProgress();
      Map<String, dynamic> token = json.decode(response.body);
      String accessToken = token['access_token'];
      print("Access token " + accessToken);
      print("userLogin " + userLogin);
//Quand tous va bien et lance la fen principale
      print("Authentification succes");

      conversionTrans(
        receiver,
        userLogin,
        password,
        accessToken,
        targetCountryIndicative,
        countrySourceCd,
        transfertMethod,
        sendingAmount,
        textNumberPhone.text,
        controllerCodepin,
      );

      hideProgress();
    } else if (response.statusCode == 401) {
//Une erreur lors de la connexion
      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "login ou mot de passe incorrect",
          duration: Toast.lengthLong, gravity: Toast.bottom);
      return throw Exception('Connexion non autorisée');
    } else if (response.statusCode == 400) {
      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "Ce compte a deja été crée",
          duration: Toast.lengthLong, gravity: Toast.bottom);

      return throw Exception('La vérification par email est requise');
    } else {
      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "Le serveur rencontre une erreur",
          duration: Toast.lengthLong, gravity: Toast.bottom);

      throw Exception('Le serveur rencontre une erreur');
    }
  }

  Future<dynamic> conversionTrans(
    String receiver,
    UserLogin,
    password,
    accessToken,
    String targetCountryIndicative,
    String countrySourceCd,
    String transfertMethod,
    double sendingAmount,
    String receiveNumberPhone,
    controllerCodepin,
  ) async {
    showProgress();
    String url =
        NolidoConstants.SERVER_URL + NolidoConstants.TRANSFERT_SIMULATE_URL;
    http.Response? response1;
    print("accessToken $accessToken");

    Uri uri1 = Uri.parse(url);
    print("uri1 ::: ");
    print(uri1);
    response1 = await http
        .post(uri1,
            headers: {
              'Content-Type': 'application/json',
              // 'cache-control': 'no-cache',
            },
            body: jsonEncode(<String, dynamic>{
              "targetCountryIndicative": targetCountryIndicative,
              "countrySourceCd": countrySourceCd,
              "transfertMethod": transfertMethod,
              "sendingAmount": sendingAmount
            }))
        .whenComplete(() => print(response1?.body));

    if (response1.statusCode == 200) {
      Map<String, dynamic> transactionInfo = json.decode(response1.body);

      print("targetCountryIndicative " +
          transactionInfo['targetCountryIndicative']);
      print("countrySourceCd " + transactionInfo['countrySourceCd']);
      print("transfertMethod " + transactionInfo['transfertMethod']);
      print(transactionInfo['sendingAmount']);

      print(transactionInfo['dueAmount']);
      print(transactionInfo['targetCountry']);
      print(transactionInfo['feeAmount']);
      print(transactionInfo['dueAmount']);
      print(transactionInfo['sendCurrency']);
      print(textNumberPhone.text);
      print(receiver);

      Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => ConfirmeTrans(
                receiver,
                ModeF,
                Mode,
                txtFullName.text,
                transactionInfo['countrySourceCd'],
                transactionInfo['targetCountry'],
                transactionInfo['targetCountryIndicative'],
                transactionInfo['receiveCurrency'],
                transactionInfo['sendCurrency'],
                transactionInfo['receiveAmount'],
                transactionInfo['feeAmount'],
                transactionInfo['dueAmount'],
                transactionInfo['sendingAmount'],
                transactionInfo['transfertMethod'],
                transactionInfo['rate'],
                transactionInfo['account'],
                receiveNumberPhone,
                UserLogin,
                password,
                controllerCodepin,
              )));
    } else if (response1.statusCode == 401) {
      print(targetCountryIndicative);
      print(countrySourceCd);
      print(transfertMethod);
      print(sendingAmount);

      print("----------------------------------------------------------");

      print("targetCountryIndicative " + targetCountryIndicative);
      print("countrySourceCd " + countrySourceCd);
      print("transfertMethod " + transfertMethod);
      print(sendingAmount);

      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "login2 ou mot de passe incorrect",
          duration: Toast.lengthLong, gravity: Toast.bottom);
      print("************************************");
      print(response1.body);
      print("************************************");

      return throw Exception('Connexion non autorisée');
    } else if (response1.statusCode == 400) {
      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "Ce compte a deja été crée",
          duration: Toast.lengthLong, gravity: Toast.bottom);
      return throw Exception('La vérification par email est requise');
    } else {
      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "Le serveur rencontre une erreur",
          duration: Toast.lengthLong, gravity: Toast.bottom);

      throw Exception('Le serveur rencontre une erreur');
    }
    hideProgress();
  }

  Future openCodeGSM(BuildContext context) async {
    return showDialog(
      barrierDismissible: true,
      context: context,
      builder: (ctx) => AlertDialog(
        title: Text(
          "Saisissez le code pin ",
          style: TextStyle(fontSize: 16),
        ),
        content: Container(
          child: TextField(
            autocorrect: true,
            controller: controllerCodepin,
            keyboardType: TextInputType.number,
            obscureText: false,
            decoration: InputDecoration(),
          ),
        ),
        actions: <Widget>[
          Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  TextButton(
                    onPressed: () => {Navigator.of(context).pop()},
                    child: const Text("Annuler"),
                  ),
                  TextButton(
                    onPressed: () => {
                      connexionToken(
                        txtFullName.text,
                        widget.userAccount.toString(),
                        widget.userPassword.toString(),
                        inPays,
                        widget.userCountryID.toString(),
                        indexMVGSM,
                        double.parse(txtMontantEnvoi.text),
                        controllerCodepin.toString(),
                      ),
                    },
                    child: const Text("Confirmer"),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
