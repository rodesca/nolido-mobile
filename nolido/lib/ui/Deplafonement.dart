import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:nolido/widget/Composant.dart';
import 'package:toast/toast.dart';

class Deplafonement extends StatefulWidget {
  const Deplafonement({Key? key}) : super(key: key);

  @override
  _DeplafonementState createState() => _DeplafonementState();
}

class _DeplafonementState extends State<Deplafonement> {
  File? imageFile;
  File? imageFileTwo;
  ImagePicker picker = ImagePicker();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Row(
              children: [
                Container(
                  padding: EdgeInsets.fromLTRB(0, 35, 0, 0),
                  child: IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: Icon(
                        Icons.arrow_back_ios_outlined,
                        color: Colors.black54,
                      )),
                )
              ],
            ),
            Column(
              children: [
                Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.fromLTRB(20, 10, 0, 0),
                  child: Column(
                    children: [
                      Text(
                        "Déplafonner",
                        style: TextStyle(
                            fontSize: 20,
                            color: Colors.black54,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Column(
              children: [
                Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.fromLTRB(20, 10, 0, 0),
                  child: Column(
                    children: [
                      Text(
                        "Mon compte",
                        style: TextStyle(
                            fontSize: 20,
                            color: Colors.black54,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 25,
            ),
            Container(
              padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: TextField(
                keyboardType: TextInputType.text,
                obscureText: false,
                decoration: InputDecoration(
                  labelText: 'Numéro de la carte',
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
              child: TextField(
                keyboardType: TextInputType.text,
                obscureText: false,
                decoration: InputDecoration(
                  labelText: 'Nom complet',
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
              child: TextField(
                keyboardType: TextInputType.text,
                obscureText: false,
                decoration: InputDecoration(
                  labelText: "Date d'établissement",
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
              child: TextField(
                keyboardType: TextInputType.text,
                obscureText: false,
                decoration: InputDecoration(
                  labelText: "Date d'expiration",
                ),
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.fromLTRB(20, 0, 0, 10),
              child: Column(
                children: [
                  SizedBox(
                    height: 25,
                  ),
                  Center(
                    child: Text(
                      "Importer votre carte d'identité ",
                      style: TextStyle(fontSize: 15, color: Colors.black54),
                    ),
                  ),
                ],
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [

                     Container(
                      width: MediaQuery.of(context).size.width / 2.7,
                      child: Card(
                        elevation: 1,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(13))),
                        child: Column(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.fromLTRB(0, 25, 0, 0),
                              child: imageFileTwo == null
                                  ? Icon(
                                Icons.camera_alt_outlined,
                                size: 35,
                              )
                                  : Image.file(imageFileTwo!),
                            ),
                            Card(
                              elevation: 5,
                              child: InkWell(
                                onTap: () {
                                  pickCamera("Recto");
                                },
                                child: Container(
                                  height: 30,
                                  width: MediaQuery.of(context).size.width,
                                  color: Colors.white24,
                                  child: Center(
                                    child: Text(
                                      'Importer(Recto)',
                                      style: TextStyle(
                                          fontSize: 13,
                                          color: Colors.black54,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  Container(
                    width: MediaQuery.of(context).size.width / 2.7,
                    child: Card(
                      elevation: 1,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(13))),
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.fromLTRB(0, 25, 0, 0),
                            child: imageFile == null
                                ? Icon(
                                    Icons.camera_alt_outlined,
                                    size: 35,
                                  )
                                : Image.file(imageFile!),
                          ),
                          Card(
                            elevation: 5,
                            child: InkWell(
                              onTap: () {
                                pickCamera("Verso");
                              },
                              child: Container(
                                height: 30,
                                width: MediaQuery.of(context).size.width,
                                color: Colors.white24,
                                child: Center(
                                  child: Text(
                                    'Importer(Verso)',
                                    style: TextStyle(
                                        fontSize: 13,
                                        color: Colors.black54,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),

            Container(
              width: MediaQuery.of(context).size.width,
              height: 85,
              padding: EdgeInsets.fromLTRB(20, 30, 20, 10),
              child: ElevatedButton(
                onPressed: () {
                  ToastContext().init(context);
                  Composant().showToast("false", "Veuiller entrez votre nom !",
                      duration: Toast.lengthLong, gravity: Toast.bottom);
                  //  Navigator.of(context)
                  //     .push(MaterialPageRoute(builder: (context) => Add()));
                },
                child: Text(
                  "Valider",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void pickCamera(String importer) async {
    XFile? chosenImage = await ImagePicker().pickImage(
        source: ImageSource.gallery, maxWidth: 1080, maxHeight: 1080);
    setState(() {
      if (chosenImage == null) {
        //Nous avons pas pu recuperer d'image
        print("Nous avons pas pu recuperer d'image");
      } else {
        if(importer == "Recto"){
          imageFileTwo = File(chosenImage!.path);
        }else{
          print("recuperer d'image");
          imageFile = File(chosenImage!.path);
        }


      }
    });
  }


}
