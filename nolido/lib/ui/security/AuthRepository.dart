import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:nolido/ui/registration/NolidoConstants.dart';
import 'package:toast/toast.dart';
import '../../widget/Composant.dart';
import '../../widget/Loading.dart';

class AuthRepository {
//Connexion
  Future<dynamic> loginConnexion(String userLogin, String password) async {
    String url =NolidoConstants.KEYCLOACK_URL;

    Map<String, String> headers = {
      'content-type': 'application/x-www-form-urlencoded',
      'cache-control': 'no-cache',
    };

    Map<String, String> data = {
      'username': userLogin,
      'password': password,
      'grant_type': 'password',
      'client_id': 'nolido-mobile-application'
    };

    Uri uri = Uri.parse(url);
    http.Response response = await http.post(uri, headers: headers, body: data);
    // print(response.statusCode);

    if (response.statusCode == 200) {
      print("Authentification succes");
    } else if (response.statusCode == 401) {
      return throw Exception('Connexion non autorisée');
    } else if (response.statusCode == 400) {
      return throw Exception('La vérification par email est requise');
    } else {
      throw Exception('Le serveur rencontre une erreur');
    }
  }

  //Anthentification
  Future<dynamic> connexion(
      String userLogin,
      String password,
      String phone,
      String nomUser,
      String countryCode,
      String motDePasse,
      String userType) async {
    String url =NolidoConstants.KEYCLOACK_URL;

    Map<String, String> headers = {
      'content-type': 'application/x-www-form-urlencoded',
      'cache-control': 'no-cache',
    };

    Map<String, String> data = {
      'username': userLogin,
      'password': password,
      'grant_type': 'password',
      'client_id': 'nolido-mobile-application'
    };

    Uri uri = Uri.parse(url);
    http.Response response = await http.post(uri, headers: headers, body: data);
    // print(response.statusCode);

    if (response.statusCode == 200) {
      Map<String, dynamic> token = json.decode(response.body);
      String accessToken = token['access_token'];
      print("Access token " + accessToken);

      String url1 = NolidoConstants.SERVER_URL+NolidoConstants.REGISTRATION_PHONE_URL;
      headers = {
        "Content-Type": "application/json",
        'cache-control': 'no-cache',
        'Accept': 'application/json',
        'Authorization': 'Bearer $accessToken'
      };

      Uri uri1 = Uri.parse(url1);
      http.Response response1 = await http
          .post(uri1,
              headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer $accessToken',
              },
              body: jsonEncode(<String, String>{
                "phone": phone,
                "login": nomUser,
                "countryCode": countryCode,
                "email": "rodesca77@gmail.com",
                "password": motDePasse,
                "userType": userType
              }))
          .whenComplete(() => print("operation ok"));
      print(response1.statusCode);
      print(response1.body);

      if (response1.statusCode == 200) {
        return token;
      } else {
        return throw Exception('Compte deja créer');
      }
    } else if (response.statusCode == 401) {
      return throw Exception('Connexion non autorisée');
    } else if (response.statusCode == 400) {
      return throw Exception('La vérification par email est requise');
    } else {
      throw Exception('Le serveur rencontre une erreur');
    }
  }

  Future<dynamic> confirmationCodePin(
    BuildContext context,
    String userLogin,
    String password,
    String codePin,
      String countryIndication,
      String phoneNumber,
  ) async {
      String url1 = NolidoConstants.SERVER_URL+NolidoConstants.REGISTRATION_VALTOKEN_URL;
      Uri uri1 = Uri.parse(url1);
      try {
        http.Response response1 = await http
            .post(uri1,
                headers: {
                  'Content-Type': 'application/json',
                  'cache-control': 'no-cache',
                  'Accept': 'application/json'
                },
                body: jsonEncode(<String, String>{
                  "token": codePin,
                  "account": userLogin,
                }))
            .whenComplete(() => print("operation ok"));
        print(response1.statusCode);
        print(response1.body);
        print("codePin $codePin");
        print("userLogin $userLogin");

        if (response1.statusCode == 200) {

          return const Loading();
        }else{
          if(response1.statusCode == 500){
            print("erreur 500");
          }
        }
      } on SocketException catch (e) {
        ToastContext().init(context);
        Composant().showToast(
            "false", "Véuillez verifier votre connexion internet",
            duration: Toast.lengthLong, gravity: Toast.bottom);
      } on Exception {
        ToastContext().init(context);
        Composant().showToast(
            "false", "Une erreur innatendue est survenue, veuillez réessayer",
            duration: Toast.lengthLong, gravity: Toast.bottom);
      }
    }
}