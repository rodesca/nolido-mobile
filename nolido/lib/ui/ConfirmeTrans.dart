import 'dart:core';
import 'dart:ui';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:nolido/ui/Accueil.dart';
import 'package:nolido/ui/TransactionSucces.dart';
import 'package:nolido/ui/registration/NolidoConstants.dart';
import 'package:nolido/widget/Composant.dart';
import 'package:toast/toast.dart';

class ConfirmeTrans extends StatefulWidget {
  String receiver;
  String? ModeF;
  String Mode;
  String txtFullName;
  double receiveAmount;
  double feeAmount;
  double dueAmount;
  String receiveCurrency;
  String sendCurrency;

  double sendingAmount;
  String transfertMethod;
  double rate;
  String? account;
  String receivePhone;
  String UserLogin;
  String password;
  String controllerCodepin;

  String countrySourceCd;
  String targetCountry;
  String targetCountryIndicative;

  ConfirmeTrans(
      this.receiver,
      this.ModeF,
      this.Mode,
      this.txtFullName,
      this.countrySourceCd,
      this.targetCountry,
      this.targetCountryIndicative,
      this.receiveCurrency,
      this.sendCurrency,
      this.receiveAmount,
      this.feeAmount,
      this.dueAmount,
      this.sendingAmount,
      this.transfertMethod,
      this.rate,
      this.account,
      this.receivePhone,
      this.UserLogin,
      this.password,
      this.controllerCodepin,
      {Key? key})
      : super(key: key);

  @override
  _ConfirmeTransState createState() => _ConfirmeTransState();
}

class _ConfirmeTransState extends State<ConfirmeTrans> {
  TextEditingController controllerNumberCarte = TextEditingController();
  TextEditingController controllerMoisCarte = TextEditingController();
  TextEditingController controllerAnneeCarte = TextEditingController();
  TextEditingController controllerCVCCarte = TextEditingController();
  bool _loading = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                Row(
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(0, 35, 0, 0),
                      child: IconButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          icon: Icon(
                            Icons.arrow_back_ios_outlined,
                            color: Colors.black54,
                          )),
                    )
                  ],
                ),
                Column(
                  children: [
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                      child: Column(
                        children: [
                          Text(
                            "Détails transactions ",
                            style: TextStyle(
                                fontSize: 23,
                                color: Colors.black54,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width / 1.1,
                      child: const Divider(
                        color: Colors.orange,
                      ),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Montant de réception : ",
                            style: TextStyle(
                                fontSize: 13,
                                color: Colors.black54,
                                fontWeight: FontWeight.normal),
                          ),
                          numberReception(
                              "",
                              "Montant de réception",
                              "${widget.receiveAmount}",
                              "${widget.receiveCurrency}"),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Montant des frais : ",
                            style: TextStyle(
                                fontSize: 13,
                                color: Colors.black54,
                                fontWeight: FontWeight.normal),
                          ),
                          numberFrais("", "Montant des frais",
                              "${widget.feeAmount}", "${widget.sendCurrency}"),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Total à payer : ",
                            style: TextStyle(
                                fontSize: 13,
                                color: Colors.black54,
                                fontWeight: FontWeight.normal),
                          ),
                          numberTotal("", "Total à payer",
                              "${widget.dueAmount}", "${widget.sendCurrency}"),
                        ],
                      ),
                    ),
                  ],
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 85,
                  padding: EdgeInsets.fromLTRB(20, 30, 20, 10),
                  child: ElevatedButton(
                    onPressed: () {
                      if (widget.ModeF == "CarteVisa" ||
                          widget.ModeF == "MasterCard") {
                        print("sendingAccount ${widget.sendingAmount}");
                        print("receiveAmount ${widget.receiveAmount}");
                        print("feeAmount ${widget.feeAmount}");
                        print("transfertMethod ${widget.transfertMethod}");
                        print("dueAmount ${widget.dueAmount}");
                        print("sendCurrency ${widget.sendCurrency}");
                        print("account ${widget.UserLogin}");
                        print("receivePhone ${widget.receivePhone}");
                        print("Mode ${widget.Mode}");
                        print("rate ${widget.rate}");
                        print("receiveCurrency ${widget.receiveCurrency}");
                        print("receiver ${widget.receiver}");

                        ModalCarte(context);
                      } else {
                        print(widget.ModeF);

                        connexionToken(
                          widget.UserLogin,
                          widget.password,
                          widget.countrySourceCd,
                          widget.targetCountry,
                          widget.targetCountryIndicative,
                          widget.sendingAmount,
                          widget.receiveAmount,
                          widget.rate,
                          widget.feeAmount,
                          widget.transfertMethod,
                          widget.dueAmount,
                          widget.sendCurrency,
                          widget.receiveCurrency,
                          widget.receivePhone,
                          widget.Mode,
                          controllerNumberCarte.text,
                          controllerMoisCarte.text,
                          controllerAnneeCarte.text,
                          controllerCVCCarte.text,
                          widget.receiver,
                          widget.controllerCodepin,
                        );
                      }
                    },
                    child: Text(
                      "Valider",
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                  ),
                ),
              ],
            ),
          ),
          new Offstage(
            offstage: _loading,
            child: _progressWidget(),
          ),
        ],
      ),
    );
  }

  Widget PropetiesCompte(String propeties) {
    return Card(
      color: Colors.white,
      elevation: 10,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      child: Container(
        margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
        width: MediaQuery.of(context).size.width / 1.1,
        height: 65,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
        ),
        child: ElevatedButton(
          onPressed: () {
            if (propeties == "Mode de financement") {
              ModalCarte(context);
            } else {}
          },
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding: EdgeInsets.fromLTRB(10, 20, 10, 0),
                    child: Text(
                      '$propeties',
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.black54,
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(10, 17, 0, 0),
                    child: Row(
                      children: [
                        propeties == "Mode de financement"
                            ? Icon(
                                Icons.mode_comment_outlined,
                                color: Colors.black54,
                                size: 22,
                              )
                            : Icon(
                                Icons.access_alarm,
                                color: Colors.black54,
                                size: 22,
                              ),
                        Icon(
                          Icons.arrow_forward_ios_rounded,
                          color: Colors.black54,
                          size: 22,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void ModalCarte(context) {
    showModalBottomSheet(
        backgroundColor: Colors.grey[800],
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(30), topLeft: Radius.circular(30)),
        ),
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return Container(
            height: MediaQuery.of(context).size.height / 2.1,
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Container(
                    child: Column(
                      children: [
                        // Composant()
                        //   .texteTF("", AppConst.last_name_destinataire),
                        SizedBox(
                          height: 15,
                        ),
                        // Composant().texteTfReadOnly(
                        //   "", AppConst.first_name_destinataire),
                      ],
                    ),
                  ),

                  Row(
                    children: [
                      Container(
                        padding: EdgeInsets.fromLTRB(60, 0, 0, 0),
                        child: Center(
                          child: Text(
                            "Informations sur les opérateurs",
                            style:
                                TextStyle(color: Colors.white70, fontSize: 16),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width / 1.2,
                    child: const Divider(
                      color: Colors.orange,
                    ),
                  ),

                  SizedBox(
                    height: 15,
                  ),

                  Container(
                    margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    height: 55,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10),
                          bottomRight: Radius.circular(10),
                          bottomLeft: Radius.circular(10)),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          alignment: Alignment.topLeft,
                          child: Image(
                            image: widget.ModeF == "MasterCard"
                                ? AssetImage("images/mastercard.png")
                                : widget.ModeF == "CarteVisa"
                                    ? AssetImage("images/visa1.png")
                                    : widget.ModeF == "ORANGE"
                                        ? AssetImage("images/orange2.png")
                                        : widget.ModeF == "FREE"
                                            ? AssetImage("images/free.png")
                                            : AssetImage("images/mtn.jpg"),
                            width: 50,
                            height: 50,
                            fit: BoxFit.contain,
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: const Divider(
                            color: Colors.orange,
                          ),
                        ),
                        Container(
                          alignment: Alignment.topRight,
                          child: Image(
                            image: widget.Mode == "MTN"
                                ? AssetImage("images/mtn.jpg")
                                : widget.Mode == "AIRTEL"
                                    ? AssetImage("images/airtel.png")
                                    : widget.Mode == "ORANGE"
                                        ? AssetImage("images/orange2.png")
                                        : widget.Mode == "FREE"
                                            ? AssetImage("images/free.png")
                                            : AssetImage("images/mtn.jpg"),
                            width: 50,
                            height: 50,
                            fit: BoxFit.contain,
                          ),
                        ),
                      ],
                    ),
                  ),

                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 85,
                    padding: EdgeInsets.fromLTRB(20, 30, 20, 0),
                    child: ElevatedButton(
                      onPressed: () {
                        if (widget.ModeF == "MasterCard" ||
                            widget.ModeF == "CarteVisa") {
                          print("controllerNumberCarte " +
                              controllerNumberCarte.text);
                          print("controllerMoisCarte " +
                              controllerMoisCarte.text);
                          print("controllerAnneeCarte " +
                              controllerAnneeCarte.text);
                          print(
                              "controllerCVCCarte " + controllerCVCCarte.text);

                          print("widget.receiver " + widget.receiver);

                          connexionToken(
                            widget.UserLogin,
                            widget.password,
                            widget.countrySourceCd,
                            widget.targetCountry,
                            widget.targetCountryIndicative,
                            widget.sendingAmount,
                            widget.receiveAmount,
                            widget.rate,
                            widget.feeAmount,
                            widget.transfertMethod,
                            widget.dueAmount,
                            widget.sendCurrency,
                            widget.receiveCurrency,
                            widget.receivePhone,
                            widget.Mode,
                            controllerNumberCarte.text,
                            controllerMoisCarte.text,
                            controllerAnneeCarte.text,
                            controllerCVCCarte.text,
                            widget.receiver,
                            widget.controllerCodepin,
                          );
                        }
                      },
                      child: Text(
                        "Confirmer la transaction",
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                    ),
                  ),

                  /*Column(
                    children: [
                      Container(
                        height: MediaQuery.of(context).size.height / 2,
                        padding: EdgeInsets.fromLTRB(25, 15, 0, 0),
                        child: Text(
                          "Conditions d'utilisation et de transfert d'argent chez NOLIDO............",
                          style: TextStyle(color: Colors.white, fontSize: 13),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ],
                  ),*/
                  //  BtnConfirmationTrans(context),
                ],
              ),
            ),
          );
        });
  }

  Widget _progressWidget() {
    return new Center(
      child: new Stack(
        children: <Widget>[
          new Container(
            color: Colors.black45,
          ),
          Center(
            child: new SpinKitSpinningLines(
              color: Colors.orangeAccent,
              size: 100,
              lineWidth: 5,
            ),
          )
        ],
      ),
    );
  }

  //Navigator.of(context)
  //  .push(MaterialPageRoute(builder: (context) => LoadingStyle()));

  void showProgress() {
    if (mounted) {
      setState(() {
        print(">>>>>> showProgress >>> _loading : ${_loading}");

        _loading = false;

        print(">>>>>> showProgress >>> _loading : ${_loading}");
      });
    }
  }

  void hideProgress() {
    if (mounted) {
      setState(() {
        print(">>>>>> hideProgress >>> ");
        _loading = true;
      });
    }
  }

  Widget numberReception(Object controler, String labelText,
          String montantReception, String receiveCurrency) =>
      TextField(
        decoration: InputDecoration(
          enabled: false,
          prefixText: "$receiveCurrency     ",
          suffixIcon: Image(
            image: widget.Mode == "MTN"
                ? AssetImage("images/mtn.jpg")
                : widget.Mode == "AIRTEL"
                    ? AssetImage("images/airtel.png")
                    : widget.Mode == "ORANGE"
                        ? AssetImage("images/orange2.png")
                        : widget.Mode == "FREE"
                            ? AssetImage("images/free.png")
                            : AssetImage("images/mtn.jpg"),
            width: 20,
            height: 20,
            fit: BoxFit.contain,
          ),
          //  border: OutlineInputBorder(),
          focusColor: Colors.orange,
          labelText: "$receiveCurrency    $montantReception",
          hintText: "Montant des " + labelText,
        ),
        textInputAction: TextInputAction.done,
        keyboardType: TextInputType.number,
        inputFormatters: <TextInputFormatter>[
          FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
        ],
      );

  Widget numberCarte(Object controler, String labelText) => Padding(
        padding: const EdgeInsets.all(8.0),
        child: TextField(
          controller: controllerNumberCarte,
          decoration: InputDecoration(
            suffixIcon: Image(
              image: AssetImage("images/visa1.png"),
              width: 10,
              height: 10,
            ),

            //  border: OutlineInputBorder(),

            focusColor: Colors.orange,
            labelText: "  " + labelText,
            hintText: "Entrer " + labelText,
          ),
          textInputAction: TextInputAction.done,
          keyboardType: TextInputType.number,

          //inputFormatters: [CurrencyTextInputFormatter(symbol: '')],
        ),
      );

  Widget number_Mois_Annee_CVC(Object controler, String labelText) =>
      SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: [
            Container(
              width: MediaQuery.of(context).size.width / 3.6,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  controller: controllerMoisCarte,
                  decoration: InputDecoration(
                    //  border: OutlineInputBorder(),
                    focusColor: Colors.orange,
                    labelText: "MM",
                    hintText: "MM",
                  ),
                  textInputAction: TextInputAction.done,
                  keyboardType: TextInputType.number,
                  //inputFormatters: [CurrencyTextInputFormatter(symbol: '')],
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width / 3.6,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  controller: controllerAnneeCarte,
                  decoration: InputDecoration(
                    //  border: OutlineInputBorder(),
                    focusColor: Colors.orange,
                    labelText: "YY",
                    hintText: "YY",
                  ),
                  textInputAction: TextInputAction.done,
                  keyboardType: TextInputType.number,
                  //inputFormatters: [CurrencyTextInputFormatter(symbol: '')],
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width / 3.6,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  controller: controllerCVCCarte,
                  decoration: InputDecoration(
                    //  border: OutlineInputBorder(),
                    focusColor: Colors.orange,
                    labelText: "CVC",
                    hintText: "CVC ",
                  ),
                  textInputAction: TextInputAction.done,
                  keyboardType: TextInputType.number,
                  //inputFormatters: [CurrencyTextInputFormatter(symbol: '')],
                ),
              ),
            ),
          ],
        ),
      );

  Widget numberFrais(Object controler, String labelText, String feeAmount,
          String sendCurrency) =>
      TextField(
        decoration: InputDecoration(
          enabled: false,
          prefixText: "$sendCurrency     ",
          //  border: OutlineInputBorder(),
          focusColor: Colors.orange,
          labelText: "$sendCurrency     $feeAmount",
          hintText: "Montant des " + labelText,
        ),
        textInputAction: TextInputAction.done,
        keyboardType: TextInputType.number,
        inputFormatters: [CurrencyTextInputFormatter(symbol: '')],
      );

  Widget numberTotal(Object controler, String labelText, String dueAmount,
          String sendCurrency) =>
      TextField(
        decoration: InputDecoration(
          enabled: false,
          prefixText: "$sendCurrency     ",
          //  border: OutlineInputBorder(),
          focusColor: Colors.orange,
          labelText: "$sendCurrency     $dueAmount",
          hintText: "Montant des " + labelText,
        ),
        textInputAction: TextInputAction.done,
        keyboardType: TextInputType.number,
        inputFormatters: [CurrencyTextInputFormatter(symbol: '')],
      );

  //Fonction permettant de se connecter a l'application
  Future<dynamic> connexionToken(
    String userLogin,
    String password,
    String countrySourceCd,
    String targetCountry,
    String targetCountryIndicative,
    double sendingAmount,
    double receiveAmount,
    double rate,
    double feeAmount,
    String transfertMethod,
    double dueAmount,
    String sendCurrency,
    String receiveCurrency,
    String receivePhone,
    String receiveMode,
    String cardNumber,
    String cardMounth,
    String cardMYear,
    String cardMCVC,
    String receiver,
    String controllerCodepin,
    //sendingAmount
  ) async {
    showProgress();
    String url = NolidoConstants.KEYCLOACK_URL;
    // 'https://www.qibisoft.com:8443/auth/realms/nolido/protocol/openid-connect/token';

    print(userLogin);

    Map<String, String> headers = {
      'content-type': 'application/x-www-form-urlencoded',
      'cache-control': 'no-cache',
    };

    Map<String, String> data = {
      'username': userLogin,
      'password': password,
      'grant_type': 'password',
      'client_id': 'nolido-mobile-application'
    };

    Uri uri = Uri.parse(url);
    http.Response response = await http.post(uri, headers: headers, body: data);
    // print(response.statusCode);
    hideProgress();
    if (response.statusCode == 200) {
      showProgress();
      Map<String, dynamic> token = json.decode(response.body);
      String accessToken = token['access_token'];
      print("Access token " + accessToken);
      print("userLogin " + userLogin);
      //Quand tous va bien et lance la fen principale
      print("Authentification succes");

      confimerTransaction(
        accessToken,
        targetCountryIndicative,
        countrySourceCd,
        targetCountry,
        sendingAmount,
        receiveAmount,
        rate,
        feeAmount,
        transfertMethod,
        dueAmount,
        sendCurrency,
        receiveCurrency,
        userLogin,
        receivePhone,
        receiveMode,
        cardNumber,
        cardMounth,
        cardMYear,
        cardMCVC,
        receiver,
        controllerCodepin,
      );
    } else if (response.statusCode == 401) {
//Une erreur lors de la connexion
      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "login ou mot de passe incorrect",
          duration: Toast.lengthLong, gravity: Toast.bottom);
      return throw Exception('Connexion non autorisée');
    } else if (response.statusCode == 400) {
      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "Ce compte a deja été crée",
          duration: Toast.lengthLong, gravity: Toast.bottom);

      return throw Exception('La vérification par email est requise');
    } else {
      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "Le serveur rencontre une erreur",
          duration: Toast.lengthLong, gravity: Toast.bottom);

      throw Exception('Le serveur rencontre une erreur');
    }
  }

  Future<dynamic> confimerTransaction(
    accessToken,
    String targetCountryIndicative,
    String countrySourceCd,
    String targetCountry,
    double sendingAmount,
    double receiveAmount,
    double rate,
    double feeAmount,
    String transfertMethod,
    double dueAmount,
    String sendCurrency,
    String receiveCurrency,
    String userLogin,
    String receivePhone,
    String operator,
    String cardNumber,
    String cardMounth,
    String cardMYear,
    String cardMCVC,
    String receiver,
    String controllerCodepin,
  ) async {
    showProgress();
    String url =
        NolidoConstants.SERVER_URL + NolidoConstants.TRANSFER_TRANSFER_URL;
    //"http://192.168.1.202:8080/v1/transfer/transfer";

    http.Response? response1;
    print("accessToken $accessToken");
    try {
      Uri uri1 = Uri.parse(url);
      response1 = await http.post(uri1,
          headers: {
            'Content-Type': 'application/json',
            //'cache-control': 'no-cache',
            //'Authorization': 'Bearer $accessToken',
          },
          body: jsonEncode(<String, dynamic>{
            "targetCountryIndicative": targetCountryIndicative,
            "countrySourceCd": countrySourceCd,
            "targetCountry": targetCountry,
            "sendingAmount": sendingAmount,
            "receiveAmount": receiveAmount,
            "rate": rate,
            "feeAmount": feeAmount,
            "transfertMethod": transfertMethod,
            "dueAmount": dueAmount,
            "sendCurrency": sendCurrency,
            "receiveCurrency": receiveCurrency,
            "account": userLogin,
            "receivePhone": receivePhone,
            "operator": operator,
            "receiver": receiver,
            "pin":controllerCodepin,
          }));
      hideProgress();
      if (response1.statusCode == 200) {
        Map<String, dynamic> transactionInfo = json.decode(response1.body);
        print("receiver    " + transactionInfo["receiver"]);

        ToastContext().init(context);
        Composant().showToast('true', "Transaction éffectuée avec succès ! ",
            duration: Toast.lengthLong, gravity: Toast.bottom);

        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => Accueil()));

        //Toast Message

      } else if (response1.statusCode == 401) {
        //Toast Message
        ToastContext().init(context);
        Composant().showToast("false", "login ou mot de passe incorrect",
            duration: Toast.lengthLong, gravity: Toast.bottom);

        return throw Exception('Connexion non autorisée');
      } else if (response1.statusCode == 400) {
        //Toast Message
        ToastContext().init(context);
        Composant().showToast("false", "Ce compte a deja été crée",
            duration: Toast.lengthLong, gravity: Toast.bottom);
        return throw Exception('La vérification par email est requise');
      } else {
        //Toast Message
        ToastContext().init(context);
        Composant().showToast("false", "Le serveur rencontre une erreur",
            duration: Toast.lengthLong, gravity: Toast.bottom);

        throw Exception('Le serveur rencontre une erreur');
      }
    } catch (e) {
      Map<String, dynamic> error = json.decode(response1!.body);
      String errorMessage = error['message'];
      print(errorMessage);

      ToastContext().init(context);
      Composant().showToast("false", errorMessage,
          duration: Toast.lengthLong, gravity: Toast.bottom);
    }
  }
}
