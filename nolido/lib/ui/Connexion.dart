import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart' as http;
import 'package:nolido/sql_helper.dart';
import 'package:nolido/ui/Accueil.dart';
import 'package:nolido/ui/Add.dart';
import 'package:nolido/ui/MdpOublier.dart';
import 'package:nolido/ui/RegistrationCode.dart';
import 'package:nolido/ui/registration/NolidoConstants.dart';
import 'package:nolido/widget/Composant.dart';
import 'package:toast/toast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';


class Connexion extends StatefulWidget {
  const Connexion({Key? key}) : super(key: key);

  @override
  _ConnexionState createState() => _ConnexionState();
}

class _ConnexionState extends State<Connexion> {
  String fullNameFind = "";
  var VarTel;
  bool _loading = true;

  TextEditingController loginController = new TextEditingController();
  TextEditingController mdpController = new TextEditingController();

  @override
  void dispose() {
    // TODO: implement dispose
    hideProgress();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.fromLTRB(20, 90, 0, 0),
                  child: const Text(
                    "Connectez-vous",
                    style: TextStyle(fontSize: 25, color: Colors.black54),
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(20, 25, 20, 0),
                  child: TextField(
                    controller: loginController,
                    obscureText: false,
                    decoration: const InputDecoration(
                      labelText: 'Login',
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(20, 25, 20, 0),
                  child: TextField(
                    controller: mdpController,
                    obscureText: true,
                    decoration: const InputDecoration(
                      labelText: 'Mot de passe',
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 85,
                  padding: EdgeInsets.fromLTRB(20, 30, 20, 10),
                  child: ElevatedButton(
                    onPressed: () {
                      loginConnexion(loginController.text, mdpController.text);
                    },
                    child: const Text(
                      "Connexion",
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                  ),
                ),
                Stack(
                  alignment: Alignment.bottomCenter,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.fromLTRB(
                          0, MediaQuery.of(context).size.height / 5, 0, 0),
                      child: Image.asset("images/GroupeConnexion.png"),
                    ),
                    Column(
                      children: [
                        Container(
                            child: ElevatedButton(
                              onPressed: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => MdpOublier()));
                              },
                              child: const Text(
                                "Mot de passe oublié ?",
                                style: TextStyle(color: Colors.white),
                              ),
                            )),
                        Container(
                            child: ElevatedButton(
                              onPressed: () {
                                Navigator.of(context).push(
                                    MaterialPageRoute(builder: (context) => Add()));
                             },
                              child: const Text(
                                "S'enregistrer",
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            )),
                        const SizedBox(
                          height: 15,
                        ),
                      ],
                    )
                  ],
                ),
              ],
            ),
          ),
          new Offstage(
            offstage: _loading,
            child: _progressWidget(),
          ),
        ],
      ),
    );
  }

  Widget _progressWidget() {
    return new Center(
      child: new Stack(
        children: <Widget>[
          new Container(
            color: Colors.black45,
          ),
          Center(
            child: new SpinKitSpinningLines(
              color: Colors.orangeAccent,
              size: 100,
              lineWidth: 5,
            ),
          )
        ],
      ),
    );
  }

  //Navigator.of(context)
  //  .push(MaterialPageRoute(builder: (context) => LoadingStyle()));

  void showProgress() {
    if (mounted) {
      setState(() {
        print(">>>>>> showProgress >>> _loading : ${_loading}");

        _loading = false;

        print(">>>>>> showProgress >>> _loading : ${_loading}");
      });
    }
  }

  void hideProgress() {
    if (mounted) {
      setState(() {
        print(">>>>>> hideProgress >>> ");
        _loading = true;
      });
    }
  }

//Fonction permettant de se connecter a l'application
  Future<dynamic> loginConnexion(String userLogin, String password) async {
    showProgress();

    String url = NolidoConstants.KEYCLOACK_URL;

    Map<String, String> headers = {
      'content-type': 'application/x-www-form-urlencoded',
      'cache-control': 'no-cache',
    };

    Map<String, String> data = {
      'username': userLogin,
      'password': password,
      'grant_type': 'password',
      'client_id': 'nolido-mobile-application'
    };

    Uri uri = Uri.parse(url);
    http.Response response = await http.post(uri, headers: headers, body: data);
    // print(response.statusCode);
    hideProgress();
    if (response.statusCode == 200) {
      showProgress();

      Map<String, dynamic> token = json.decode(response.body);
      String accessToken = token['access_token'];
      print("Access token " + accessToken);
      print("userLogin " + userLogin);
//Quand tous va bien et lance la fen principale
      print("Authentification succes");

      findUsrName(accessToken, userLogin, password);

    } else if (response.statusCode == 401) {
//Une erreur lors de la connexion
      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "login ou mot de passe incorrect",
          duration: Toast.lengthLong, gravity: Toast.bottom);
      return throw Exception('Connexion non autorisée');
    } else if (response.statusCode == 400) {
      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "Ce compte a deja été crée",
          duration: Toast.lengthLong, gravity: Toast.bottom);

      return throw Exception('La vérification par email est requise');
    } else {
      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "Le serveur rencontre une erreur",
          duration: Toast.lengthLong, gravity: Toast.bottom);

      throw Exception('Le serveur rencontre une erreur');
    }

  }

  Future<dynamic> findUsrName(
      accessToken, String userLogin, String password) async {
    String url = NolidoConstants.SERVER_URL +
        NolidoConstants.REGISTRATION_URL +
        NolidoConstants.INFO_URL;

    http.Response? response1;

      Uri uri1 = Uri.parse(url);
      response1 = await http.post(uri1,
          headers: {
            'Content-Type': 'application/json',
            'cache-control': 'no-cache',
          //  'Authorization': 'Bearer $accessToken',
          },
          body: jsonEncode(<String, String>{
            "account": userLogin,
          }));

      if (response1.statusCode == 200) {
        Map<String, dynamic> userInfo = json.decode(response1.body);
        SQLHelper sqlHelper = new SQLHelper();
        // sqlHelper.createUsr(userInfo['userId'], userInfo['account'], userInfo['currency'], userInfo['countryIndicative'], userInfo['fullName'],userInfo['countryID']);
        print(userLogin);
        //prefs = await SharedPreferences.getInstance();
        //prefs.setString("username", "naresh");
        SharedPreferences prefs = await SharedPreferences.getInstance();

        prefs.setString("fullName", userInfo['fullName']);
        prefs.setString("currency", userInfo['currency']);
        prefs.setString("countryIndicative", userInfo["countryIndicative"]);
        prefs.setString("phone", userInfo["phone"]);
        prefs.setString("countryID", userInfo["countryID"]);
        prefs.setString("account", userInfo["account"]);

        prefs.setString("userLogin", userLogin);
        prefs.setString("password", password);

        hideProgress();
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => Accueil()));
      } else if (response1.statusCode == 401) {
        //Toast Message
        ToastContext().init(context);
        Composant().showToast("false", "login ou mot de passe incorrect",
            duration: Toast.lengthLong, gravity: Toast.bottom);

        return throw Exception('Connexion non autorisée');
      } else if (response1.statusCode == 400) {
        //Toast Message
        ToastContext().init(context);
        Composant().showToast("false", "Ce compte a deja été crée",
            duration: Toast.lengthLong, gravity: Toast.bottom);
        return throw Exception('La vérification par email est requise');
      } else {
        //Toast Message
        ToastContext().init(context);
        Composant().showToast("false", "Le serveur rencontre une erreur",
            duration: Toast.lengthLong, gravity: Toast.bottom);

        throw Exception('Le serveur rencontre une erreur');
      }
    }
}
