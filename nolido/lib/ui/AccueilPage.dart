import 'dart:convert';

import 'package:dot_navigation_bar/dot_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'package:nolido/ui/Deplafonement.dart';
import 'package:nolido/ui/DetailTransfert.dart';
import 'package:nolido/ui/EffectuerTrans.dart';
import 'package:http/http.dart' as http;
import 'package:nolido/ui/Profil.dart';
import 'package:nolido/ui/Transaction.dart';
import 'package:nolido/ui/registration/NolidoConstants.dart';
import 'package:nolido/ui/transfert/UserTransaction.dart';
import 'package:nolido/widget/Composant.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

import 'dart:async';
import 'Accueil.dart';
import 'transfert/UserTransaction.dart';

class AccueilPage extends StatefulWidget {
  String fullName = "";
  String password = "";
  String account = "";
  String phone = "";

  AccueilPage({Key? key}) : super(key: key) {
    getSharedPrefs();
  }

  Future<Null> getSharedPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    this.fullName = prefs.getString("fullName")!;
    this.password = prefs.getString("password")!;
    this.account = prefs.getString("account")!;
    this.phone = prefs.getString("phone")!;
  }

  @override
  _AccueilPageState createState() => _AccueilPageState();
}

class _AccueilPageState extends State<AccueilPage> {
  late Future<List<UserTransaction>> futureData;
  Future refreshList() async {
    await Future.delayed(Duration(seconds: 2));
  }

  int currentIndex = 0;
  @override
  final screens = [
    AccueilPage(),
    Transaction(),
    Profil(),
  ];

  bool _loading = true;
  List<UserTransaction> postModel = [];
  Map mapLis2 = {};

  // Future trans() async {
  // mapLis2 = await connexionToken(widget.account, widget.password);
  // print(mapLis2);
  //}

  @override
  initState() {
    super.initState();
    setState(() {
      showProgress();
      futureData = connexionToken(widget.account, widget.password);
      if (futureData == null) {
        futureData = [] as Future<List<UserTransaction>>;

        Timer(Duration(seconds: 2), hideProgress);
      }
      Timer(Duration(seconds: 2), hideProgress);
    });
  }

  Future<Null> getSharedPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    widget.fullName = prefs.getString("fullName")!;
    widget.password = prefs.getString("password")!;
    widget.account = prefs.getString("account")!;
    widget.phone = prefs.getString("phone")!;
  }

//Detail Variable
  String Detail_transfertDate = "";
  String countrySourceCd = "";
  double sendingAmount = 0;
  double feeAmount = 0;
  double dueAmount = 0;
  String transfertMethod = "";
  String sendCurrency = "";
  String receiveCurrency = "";
  String receivePhone = "";
  String operator = "";
  String receiver = "";
  final formatCurrency = new NumberFormat("#,##0.00");
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              children: [
                Row(
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(25, 45, 0, 0),
                      child: CircleAvatar(
                        radius: 30,
                        child: Image.asset("images/picture.png"),
                      ),
                    ),
                    const SizedBox(
                      width: 12,
                    ),
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: EdgeInsets.fromLTRB(0, 60, 0, 0),
                            child: Text(
                              'Salut, ' + widget.fullName.toString().substring(0, 7) + '...',
                              overflow: TextOverflow.ellipsis,
                              style: const TextStyle(
                                  fontSize: 12,
                                  color: Colors.black54,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                            child:  Text(
                              "Bienvenue sur Nolido",
                              style: TextStyle(
                                  fontSize: 10, color: Colors.black54),
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width / 1.7,
                            child: const Divider(
                              color: Colors.grey,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                 SingleChildScrollView(
                    scrollDirection: Axis.horizontal,

                    child: Container(

                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          TextButton(
                            onPressed: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => EffectuerTrans()));
                              print("Transfert");
                            },
                            child: Container(
                              color: Colors.white,
                              padding: EdgeInsets.fromLTRB(5, 20, 20, 0),
                              width: MediaQuery.of(context).size.width / 2.3,
                              child: Card(
                                elevation: 5,
                                shape: const RoundedRectangleBorder(
                                    side:
                                    BorderSide(color: Colors.orange, width: 3),
                                    borderRadius:
                                    BorderRadius.all(Radius.circular(15))),
                                shadowColor: Colors.green[100],
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.fromLTRB(10, 15, 0, 10),
                                      child: Image.asset("images/Transfer.png"),
                                    ),
                                    Container(
                                        padding: EdgeInsets.fromLTRB(0, 15, 0, 10),
                                        child: Column(
                                          children: const [
                                            Text(
                                              'TRANSFERT',
                                              style: TextStyle(
                                                  fontSize: 15,
                                                  color: Colors.black54,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Text(
                                              'argent',
                                              style: TextStyle(
                                                fontSize: 15,
                                                color: Colors.black54,
                                              ),
                                            ),
                                          ],
                                        ))
                                  ],
                                ),
                              ),
                            ),
                          ),
                          TextButton(
                            onPressed: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => Deplafonement()));
                              print("Echanger");
                            },
                            child: Container(
                              color: Colors.white,
                              padding: EdgeInsets.fromLTRB(10, 20, 20, 0),
                              width: MediaQuery.of(context).size.width / 2.3,
                              child: Card(
                                elevation: 5,
                                shape: const RoundedRectangleBorder(
                                    side:
                                    BorderSide(color: Colors.orange, width: 3),
                                    borderRadius:
                                    BorderRadius.all(Radius.circular(15))),
                                shadowColor: Colors.green[100],
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.fromLTRB(0, 15, 0, 10),
                                      child: Image.asset("images/Transfer.png"),
                                    ),
                                    Container(
                                        padding: EdgeInsets.fromLTRB(0, 15, 0, 10),
                                        child: Column(
                                          children: const [
                                            Text(
                                              'DEPLAFONNER',
                                              style: TextStyle(
                                                  fontSize: 15,
                                                  color: Colors.black54,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Text(
                                              'Mon compte',
                                              style: TextStyle(
                                                fontSize: 15,
                                                color: Colors.black54,
                                              ),
                                            ),
                                          ],
                                        ))
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.fromLTRB(20, 20, 0, 0),
                  child: Text(
                    "Transactions récentes",
                    style: TextStyle(fontSize: 15, color: Colors.black54),
                  ),
                ),
                RefreshIndicator(
                    child: SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: FutureBuilder<List<UserTransaction>>(
                            future: futureData,
                            builder: (context, snapshot) {
                              if (snapshot.hasData) {
                                List<UserTransaction>? data = snapshot.data;
                                return Container(
                                  height:
                                      MediaQuery.of(context).size.height / 2,
                                  color: Colors.white,
                                  child: ListView.builder(
                                      itemCount: data?.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        return Container(
                                          height: 85,
                                          color: Colors.white,
                                          child: Center(
                                            child: TransactionRecentes(
                                              data![index].amount,
                                              data![index].receiver,
                                              data![index].operator,
                                              data![index].sendingCurrency,
                                              data![index].transferId,
                                              widget.account,
                                              widget.password,
                                            ),
                                          ),
                                        );
                                      }),
                                );
                              } else if (!snapshot.hasData) {
                                return Column(
                                  children: [
                                    SizedBox(
                                      height: 80,
                                    ),
                                    Image.asset(
                                      'images/noData.gif',
                                      height: 250,
                                    ),
                                  ],
                                );
                              } else if (snapshot.hasError) {
                                return Image.asset('images/noInternet.gif');
                              }
                              return CircularProgressIndicator();
                            })),
                    onRefresh: refreshList),
                SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
          new Offstage(
            offstage: _loading,
            child: _progressWidget(),
          ),
        ],
      ),
    );
  }

  Widget TransactionRecentes(
      double montant,
      String beneficiaire,
      String operateur,
      String sendingCurrency,
      String transferId,
      String account,
      String password) {
    return SingleChildScrollView(
      child: Card(
        color: Colors.white,
        elevation: 2,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        child: Container(
          margin: EdgeInsets.fromLTRB(8, 0, 0, 2),
          width: MediaQuery.of(context).size.width / 1.1,
          height: 70,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
          ),
          child: ElevatedButton(
            onPressed: () {
              //ModalDetail(context);
              connexionTokenDetail(transferId, account, password);
            },
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: Text(
                        'Transféré de ',
                        style: TextStyle(
                            fontSize: 15,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                      child: Text(
                        '$sendingCurrency ' + formatCurrency.format(montant),
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 15,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(30, 15, 10, 0),
                      child: Text(
                        '$operateur',
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 15,
                            color: Colors.black54,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(10, 15, 10, 0),
                      child: SingleChildScrollView(
                        child: Text(
                          '' + beneficiaire.toString().substring(0, 3) + '...',
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontSize: 15,
                              color: Colors.black54,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _progressWidget() {
    return new Center(
      child: new Stack(
        children: <Widget>[
          new Container(
            color: Colors.black45,
          ),
          Center(
            child: new SpinKitSpinningLines(
              color: Colors.orangeAccent,
              size: 100,
              lineWidth: 5,
            ),
          )
        ],
      ),
    );
  }

  //Navigator.of(context)
  //  .push(MaterialPageRoute(builder: (context) => LoadingStyle()));

  void showProgress() {
    if (mounted) {
      setState(() {
        print(">>>>>> showProgress >>> _loading : ${_loading}");

        _loading = false;

        print(">>>>>> showProgress >>> _loading : ${_loading}");
      });
    }
  }

  void hideProgress() {
    if (mounted) {
      setState(() {
        print(">>>>>> hideProgress >>> ");
        _loading = true;
      });
    }
  }

  //Fonction permettant de se connecter a l'application
  Future<List<UserTransaction>> connexionToken(
    String account,
    String password,
  ) async {
    showProgress();
    String url = NolidoConstants.KEYCLOACK_URL;
    // 'https://www.qibisoft.com:8443/auth/realms/nolido/protocol/openid-connect/token';

    print(account);

    Map<String, String> headers = {
      'content-type': 'application/x-www-form-urlencoded',
      'cache-control': 'no-cache',
    };

    Map<String, String> data = {
      'username': account,
      'password': password,
      'grant_type': 'password',
      'client_id': 'nolido-mobile-application'
    };

    Uri uri = Uri.parse(url);
    http.Response response = await http.post(uri, headers: headers, body: data);
    // print(response.statusCode);
    hideProgress();
    if (response.statusCode == 200) {
      showProgress();
      Map<String, dynamic> token = json.decode(response.body);
      String accessToken = token['access_token'];
      print("Access token " + accessToken);
      print("userLogin " + account);
      //Quand tous va bien et lance la fen principale
      print("Authentification succes");

      return fechListTrans(account, password, accessToken);
    } else if (response.statusCode == 401) {
//Une erreur lors de la connexion
      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "login1 ou mot de passe incorrect",
          duration: Toast.lengthLong, gravity: Toast.bottom);
      return throw Exception('Connexion non autorisée');
    } else if (response.statusCode == 400) {
      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "Ce compte a deja été crée",
          duration: Toast.lengthLong, gravity: Toast.bottom);

      return throw Exception('La vérification par email est requise');
    } else {
      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "Le serveur rencontre une erreur",
          duration: Toast.lengthLong, gravity: Toast.bottom);

      throw Exception('Le serveur rencontre une erreur');
    }
  }

  Future<List<UserTransaction>> fechListTrans(
    userLogin,
    password,
    accessToken,
  ) async {
    showProgress();
    String url =
        NolidoConstants.SERVER_URL + NolidoConstants.TRANSFER_RECENTS_URL;
    //'http://192.168.1.202:8080/v1/transfer/recents';

    print("accessToken $accessToken");
    try {
      Uri uri1 = Uri.parse(url);
      final response1 = await http.post(uri1,
          headers: {
            'Content-Type': 'application/json',
            //NolidoConstants.NO_CACHE: NolidoConstants.NO_CACHE,
            //NolidoConstants.ACCEPT: NolidoConstants.JSON,
            //NolidoConstants.AUTHORIZATION: 'Bearer  $accessToken',
          },
          body: jsonEncode(<String, dynamic>{
            "account": userLogin,
          }));
      hideProgress();

      if (response1.statusCode == 200) {
        List jsonResponse = json.decode(response1.body);
        return jsonResponse
            .map((data) => new UserTransaction.fromJson(data))
            .toList();
      } else if (response1.statusCode == 401) {
        print("userLogin "+userLogin);
        print("password "+password);

        //Toast Message
        ToastContext().init(context);
        Composant().showToast("false", "login3 ou mot de passe incorrect",
            duration: Toast.lengthLong, gravity: Toast.bottom);

        return throw Exception('Connexion non autorisée');
      } else if (response1.statusCode == 400) {
        //Toast Message
        ToastContext().init(context);
        Composant().showToast("false", "Ce compte a deja été crée",
            duration: Toast.lengthLong, gravity: Toast.bottom);
        return throw Exception('La vérification par email est requise');
      } else {
        //Toast Message
        ToastContext().init(context);
        Composant().showToast("false", "Le serveur rencontre une erreur",
            duration: Toast.lengthLong, gravity: Toast.bottom);

        throw Exception('Le serveur rencontre une erreur');
      }
    } catch (e) {
      //  Map<String, dynamic> error = json.decode(response1!.body);
      // String errorMessage = error['message'];
      // print(errorMessage);

      /*  ToastContext().init(context);
      Composant().showToast("false", "Erreur",
          duration: Toast.lengthLong, gravity: Toast.bottom);*/
      throw Exception('Le serveur rencontre une erreur');
    }
  }

  //Fonction permettant de se connecter a l'application
  Future<dynamic> connexionTokenDetail(
    String transfertCd,
    String userLogin,
    String password,
  ) async {
    showProgress();
    String url = NolidoConstants.KEYCLOACK_URL;
    // 'https://www.qibisoft.com:8443/auth/realms/nolido/protocol/openid-connect/token';

    print(userLogin);

    Map<String, String> headers = {
      'content-type': 'application/x-www-form-urlencoded',
      'cache-control': 'no-cache',
    };

    Map<String, String> data = {
      'username': userLogin,
      'password': password,
      'grant_type': 'password',
      'client_id': 'nolido-mobile-application'
    };

    Uri uri = Uri.parse(url);
    http.Response response = await http.post(uri, headers: headers, body: data);
    // print(response.statusCode);
    hideProgress();
    if (response.statusCode == 200) {
      showProgress();
      Map<String, dynamic> token = json.decode(response.body);
      String accessToken = token['access_token'];
      print("Access token " + accessToken);
      print("userLogin " + userLogin);
      //Quand tous va bien et lance la fen principale
      print("Authentification succes");

      fechDetailTrans(transfertCd, accessToken);
    } else if (response.statusCode == 401) {
//Une erreur lors de la connexion
      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "login3 ou mot de passe incorrect",
          duration: Toast.lengthLong, gravity: Toast.bottom);
      return throw Exception('Connexion non autorisée');
    } else if (response.statusCode == 400) {
      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "Ce compte a deja été crée",
          duration: Toast.lengthLong, gravity: Toast.bottom);

      return throw Exception('La vérification par email est requise');
    } else {
      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "Le serveur rencontre une erreur",
          duration: Toast.lengthLong, gravity: Toast.bottom);

      throw Exception('Le serveur rencontre une erreur');
    }
  }

  Future<dynamic> fechDetailTrans(
    String transfertCd,
    String accessToken,
  ) async {
    showProgress();
    String url =
        NolidoConstants.SERVER_URL + NolidoConstants.TRANSFER_DISPLAY_URL;
    //'http://192.168.1.202:8080/v1/transfer/display';

    print("accessToken $accessToken");
    try {
      Uri uri1 = Uri.parse(url);
      final response1 = await http.post(uri1,
          headers: {
            'Content-Type': 'application/json',
            //'cache-control': 'no-cache',
            //'Authorization': 'Bearer $accessToken',
          },
          body: jsonEncode(<String, String>{
            "transfertCd": transfertCd,
          }));
      hideProgress();

      print("accessToken  " + accessToken);
      print("Numero transfert  " + transfertCd);

      if (response1.statusCode == 200) {
        Map<dynamic, dynamic> transInfo = json.decode(response1.body);

        print(transInfo);
        print('ici');

        print(transInfo['transfertDate']);
        print(transInfo['transfertMethod']);
        print(transInfo['targetCountryIndicative']);
        print(transInfo['countrySourceCd']);
        print(transInfo['sendingAmount']);
        print(transInfo['rate']);
        print(transInfo['receiveAmount']);
        print(transInfo['feeAmount']);
        print(transInfo['dueAmount']);
        print(transInfo['sendCurrency']);
        print(transInfo['receiveCurrency']);
        print(transInfo['receivePhone']);
        print(transInfo['operator']);
        print(transInfo['targetCountry']);
        print(transInfo['receiver']);

        Navigator.push(context, MaterialPageRoute(builder: (context) {
          return DetailTransfert(
              transInfo['transfertDate'],
              transInfo['transfertMethod'],
              transInfo['countrySourceCd'],
              transInfo['sendingAmount'],
              transInfo['rate'],
              transInfo['receiveAmount'],
              transInfo['feeAmount'],
              transInfo['dueAmount'],
              transInfo['sendCurrency'],
              transInfo['receiveCurrency'],
              transInfo['receivePhone'],
              transInfo['operator'],
              transInfo['targetCountry'],
              transInfo['receiver'],
          );
        }));
      } else if (response1.statusCode == 401) {
        //Toast Message
        ToastContext().init(context);
        Composant().showToast("false", "login4 ou mot de passe incorrect",
            duration: Toast.lengthLong, gravity: Toast.bottom);

        return throw Exception('Connexion non autorisée');
      } else if (response1.statusCode == 400) {
        //Toast Message
        ToastContext().init(context);
        Composant().showToast("false", "Ce compte a deja été crée",
            duration: Toast.lengthLong, gravity: Toast.bottom);
        return throw Exception('La vérification par email est requise');
      } else {
        //Toast Message
        ToastContext().init(context);
        Composant().showToast("false", "Le serveur rencontre une erreur",
            duration: Toast.lengthLong, gravity: Toast.bottom);

        throw Exception('Le serveur rencontre une erreur');
      }
    } catch (e) {
      //  Map<String, dynamic> error = json.decode(response1!.body);
      // String errorMessage = error['message'];
      // print(errorMessage);

      /*  ToastContext().init(context);
      Composant().showToast("false", "Erreur",
          duration: Toast.lengthLong, gravity: Toast.bottom);*/
      throw Exception('Le serveur rencontre une erreur');
    }
  }

}

class _AccueilState extends State<Accueil> {
  String? e;
  String? fullNameFind;
  _AccueilState(fullNameFind) {
    this.fullNameFind = fullNameFind;
  }
  var _selectedTab = _SelectedTab.home;

  void _handleIndexChanged(int i) {
    setState(() {
      _selectedTab = _SelectedTab.values[i];
    });
  }

  int currentIndex = 0;

  final screens = [
    AccueilPage(),
    Transaction(),
    Profil(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      body: screens[currentIndex],
      bottomNavigationBar: DotNavigationBar(
        currentIndex: currentIndex,
        selectedItemColor: Colors.orange,
        unselectedItemColor: Colors.black,
        onTap: (index) => setState(() => currentIndex = index),
        backgroundColor: Colors.grey,
        // dotIndicatorColor: Colors.black,
        items: [
          /// Home
          DotNavigationBarItem(
            icon: Icon(
              Icons.home_outlined,
              size: 26,
            ),
            unselectedColor: Colors.white,
            selectedColor: Colors.black,
          ),

          /// Search
          DotNavigationBarItem(
            icon: Icon(Icons.access_alarm, size: 26),
            unselectedColor: Colors.white,
            selectedColor: Colors.black,
          ),

          /// Profile
          DotNavigationBarItem(
            icon: Icon(Icons.perm_identity_sharp, size: 26),
            unselectedColor: Colors.white,
            selectedColor: Colors.black,
          ),
        ],
      ),
    );
  }
}

enum _SelectedTab { home, favorite, search, person }
