import 'dart:async';

import 'package:dot_navigation_bar/dot_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:nolido/ui/AccueilPage.dart';
import 'package:nolido/ui/Profil.dart';
import 'package:nolido/ui/Transaction.dart';

class Accueil extends StatefulWidget {
  String? fullNameFind;
  String? account;
  String? password;
  String? phone;

  Accueil({Key? key}) : super(key: key);

  @override
  _AccueilState createState() => _AccueilState(fullNameFind, account, password, phone);
}

class _AccueilState extends State<Accueil> {
  String? e;
  String? fullNameFind;
  String? account;
  String? password;
  String? phone;

  _AccueilState(fullNameFind, account, password,  phone) {
    this.fullNameFind = fullNameFind;
    this.account = account;
    this.password = password;
    this.phone = phone;
  }
  var _selectedTab = _SelectedTab.home;

  void _handleIndexChanged(int i) {
    setState(() {
      _selectedTab = _SelectedTab.values[i];
    });
  }

  int currentIndex = 2;

  void refeshAuto (){
    setState(() {
      currentIndex = 0;
    });

  }
  @override
  void initState() {
    // TODO: implement initState

    setState(() {
      Timer(Duration(microseconds: 1), refeshAuto);
    });
  }

  final screens = [
    AccueilPage(),
    Transaction(),
    Profil(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      body: screens[currentIndex],
      bottomNavigationBar: DotNavigationBar(
        currentIndex: currentIndex,
        selectedItemColor: Colors.orange,
        unselectedItemColor: Colors.black,
        onTap: (index) => setState(() => currentIndex = index),
        backgroundColor: Colors.grey,
        // dotIndicatorColor: Colors.black,
        items: [
          /// Home
          DotNavigationBarItem(
            icon: Icon(
              Icons.home_outlined,
              size: 26,
            ),
            unselectedColor: Colors.white,
            selectedColor: Colors.black,
          ),

          /// Search
          DotNavigationBarItem(
            icon: Icon(Icons.access_alarm, size: 26),
            unselectedColor: Colors.white,
            selectedColor: Colors.black,
          ),

          /// Profile+
          DotNavigationBarItem(
            icon: Icon(Icons.perm_identity_sharp, size: 26),
            unselectedColor: Colors.white,
            selectedColor: Colors.black,
          ),
        ],
      ),
    );
  }
}

enum _SelectedTab { home, favorite, search, person }
