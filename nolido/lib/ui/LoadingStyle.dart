import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:nolido/ui/Connexion.dart';
import 'package:nolido/widget/Composant.dart';
import 'package:toast/toast.dart';

class LoadingStyle extends StatefulWidget {
  const LoadingStyle({Key? key}) : super(key: key);

  @override
  _LoadingStyleState createState() => _LoadingStyleState();
}

class _LoadingStyleState extends State<LoadingStyle> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Timer(Duration(seconds: 2), callConnexionErreur);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: loading(),
          ),
        ],
      ),
    );
  }

  callback() {
    //Toast Message
    ToastContext().init(context);
    Composant().showToast("true", "Votre compte nolido à été crée",
        duration: Toast.lengthLong, gravity: Toast.bottom);
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => Connexion()));
  }

  callConnexionErreur() {
    Navigator.pop(context);
  }

  Widget loading() {
    return SpinKitSpinningLines(
      color: Colors.orangeAccent,
      size: 100,
      duration: Duration(seconds: 1),
      lineWidth: 5,
    );
  }
}
