import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:nolido/ui/registration/Registration.dart';
import 'package:nolido/ui/security/AuthRepository.dart';
import 'package:nolido/widget/Composant.dart';
import 'package:nolido/widget/Loading.dart';
import 'package:toast/toast.dart';

class RegistrationCode extends StatefulWidget {
  late String? _password;
  late String? _login;
  late String? _countryIndication;
  late String? _phoneNumber;
  RegistrationCode(this._login,this._password, this._countryIndication, this._phoneNumber, {Key? key}) : super(key: key);

  @override
  State<RegistrationCode> createState() => _RegistrationCodeState();
}

class _RegistrationCodeState extends State<RegistrationCode> {
  TextEditingController _codeRegistration1 = new TextEditingController();
  TextEditingController _codeRegistration2 = new TextEditingController();
  TextEditingController _codeRegistration3 = new TextEditingController();
  TextEditingController _codeRegistration4 = new TextEditingController();
  bool _loading = true;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(

              children: [
                Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.fromLTRB(20, 90, 0, 0),
                  child: const Text(
                    "Entrer votre code d'activation",
                    style: TextStyle(fontSize: 20, color: Colors.black54),
                  ),
                ),
                Center(
                  child: Column(
                    children: [
                      Row(

                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [


                          Container(
                            padding: EdgeInsets.fromLTRB(20, 25, 20, 0),
                            width: 80,
                            child: TextField(
                              maxLength: 1,
                              textAlign: TextAlign.center,
                              controller: _codeRegistration1,
                              obscureText: false,
                            ),
                          ),

                          Container(
                            padding: EdgeInsets.fromLTRB(20, 25, 20, 0),
                            width: 80,
                            child: TextField(
                              textAlign: TextAlign.center,
                              maxLength: 1,
                              controller: _codeRegistration2,
                              obscureText: false,
                            ),
                          ),

                          Container(
                            padding: EdgeInsets.fromLTRB(20, 25, 20, 0),
                            width: 80,
                            child: TextField(
                              textAlign: TextAlign.center,
                              maxLength: 1,
                              controller: _codeRegistration3,

                              obscureText: false,
                            ),
                          ),

                          Container(
                            padding: EdgeInsets.fromLTRB(20, 25, 20, 0),
                            width: 80,
                            child: TextField(
                              textAlign: TextAlign.center,
                              maxLength: 1,
                              controller: _codeRegistration4,
                              obscureText: false,
                            ),
                          ),
                        ],
                      ),

                      Row(

                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Container(
                              padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                              child: ElevatedButton(
                                onPressed: () {

                                  if((_codeRegistration1.text!=null || _codeRegistration1.text.isNotEmpty)
                                      && (_codeRegistration2.text!= null || _codeRegistration2.text.isNotEmpty)
                                      && (_codeRegistration3.text!=null || _codeRegistration3.text.isNotEmpty)
                                      && (_codeRegistration4.text!=null || _codeRegistration4.text.isNotEmpty)){

                                    //registrationRenvoyer(context);
                                    print("En développement");

                                  }else{
                                    ToastContext().init(context);
                                    Composant().showToast("false", "Entrer votre code d'activation",
                                        duration: Toast.lengthLong, gravity: Toast.bottom);
                                  }
                                },
                                child: const Text(
                                  "Renvoyer",
                                  style: TextStyle(
                                    color: Colors.black54,
                                  ),
                                ),
                              )),

                          Container(
                              padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                              child: ElevatedButton(
                                onPressed: () {
                                  if((_codeRegistration1.text!=null || _codeRegistration1.text.isNotEmpty)
                                      && (_codeRegistration2.text!= null || _codeRegistration2.text.isNotEmpty)
                                      && (_codeRegistration3.text!=null || _codeRegistration3.text.isNotEmpty)
                                      && (_codeRegistration4.text!=null || _codeRegistration4.text.isNotEmpty)){

                                    String txtActivationCode = _codeRegistration1.text+_codeRegistration2.text+_codeRegistration3.text+_codeRegistration4.text;

                                    if(txtActivationCode != null || !txtActivationCode.isEmpty){
                                      print('txtActivationCode ' + txtActivationCode);
                                      print('login ${widget._login}');
                                      print('password ${widget._password}');
                                      print('login ${widget._countryIndication}');
                                      print('password ${widget._phoneNumber}');

                                      AuthRepository().confirmationCodePin(context, widget._login!, widget._password!, txtActivationCode,
                                        widget._countryIndication!, widget._phoneNumber!);
                                      Navigator.of(context)
                                          .push(MaterialPageRoute(builder: (context) => Loading()));
                                      print("attente de confirmation");
                                    }else{
                                      ToastContext().init(context);
                                      Composant().showToast("false", "Entrer votre code d'activation",
                                          duration: Toast.lengthLong, gravity: Toast.bottom);
                                    }
                                  }else{
                                    ToastContext().init(context);
                                    Composant().showToast("false", "Entrer votre code d'activation",
                                        duration: Toast.lengthLong, gravity: Toast.bottom);
                                  }
                                },
                                child: const Text(
                                  "Confirmer",
                                  style: TextStyle(
                                    color: Colors.black54,
                                  ),
                                ),
                              )),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          new Offstage(
            offstage: _loading,
            child: _progressWidget(),
          ),
        ],
      ),
    );
  }

  Widget _progressWidget() {
    return new Center(
      child: new Stack(
        children: <Widget>[
          new Container(
            color: Colors.black45,
          ),
          Center(
            child: new SpinKitSpinningLines(
              color: Colors.orangeAccent,
              size: 100,
              lineWidth: 5,
            ),
          )
        ],
      ),
    );
  }

  //Navigator.of(context)
  //  .push(MaterialPageRoute(builder: (context) => LoadingStyle()));

  void showProgress() {
    if (mounted) {
      setState(() {
        print(">>>>>> showProgress >>> _loading : ${_loading}");

        _loading = false;

        print(">>>>>> showProgress >>> _loading : ${_loading}");
      });
    }
  }

  void hideProgress() {
    if (mounted) {
      setState(() {
        print(">>>>>> hideProgress >>> ");
        _loading = true;
      });
    }
  }


}
