import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:nolido/ui/Connexion.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(
                          10, MediaQuery.of(context).size.height / 25, 0, 0),
                      child: Column(
                        children: [
                          Text(
                            "Rapide",
                            style: TextStyle(
                                fontSize: 30,
                                fontWeight: FontWeight.bold,
                                color: Colors.black54),
                          ),
                          Text(
                            " et facile",
                            style: TextStyle(
                                fontSize: 30,
                                fontWeight: FontWeight.bold,
                                color: Colors.black54),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(width: 50,),
                    Container(
                      padding: EdgeInsets.fromLTRB(0, 40, 0, 0),
                      child: Image.asset("images/LOGO_NOLIDO.png",height: 160,width: 160,),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Image.asset("images/picture.png"),
              SizedBox(
                height: 10,
              ),
              Container(
                child: Text("data"),
              ),

              Container(
                width: MediaQuery.of(context).size.width ,
                height: 80,
                padding: EdgeInsets.fromLTRB(30, 30, 30, 10),
                child: ElevatedButton(

                    onPressed: (){
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => Connexion()));
                    },
                    child: Text("Démarrer", style: TextStyle(color: Colors.white, fontSize: 20),),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
