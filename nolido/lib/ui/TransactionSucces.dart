import 'package:flutter/material.dart';
import 'package:nolido/ui/Accueil.dart';
import 'package:nolido/ui/AccueilPage.dart';
import 'package:nolido/widget/Composant.dart';
import 'package:toast/toast.dart';

class TransactionSucces extends StatefulWidget {
  const TransactionSucces({Key? key}) : super(key: key);

  @override
  _TransactionSuccesState createState() => _TransactionSuccesState();
}

class _TransactionSuccesState extends State<TransactionSucces> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      //Toast Message
      ToastContext().init(context);
      Composant().showToast('true', "Transaction reussie ",
          duration: Toast.lengthLong, gravity: Toast.bottom);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Center(
              child: Column(
                children: [
                  Center(
                    child: Image.asset(
                      "images/trophy-winner.gif",
                      height: 125.0,
                      width: 125.0,
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 85,
                    padding: EdgeInsets.fromLTRB(20, 30, 20, 10),
                    child: ElevatedButton(
                      onPressed: () {

                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => Accueil()));

                      },
                      child: Text(
                        "Accueil",
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
