import 'dart:async';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'package:nolido/ui/DetailTransfert.dart';
import 'package:nolido/ui/registration/NolidoConstants.dart';
import 'package:nolido/ui/transfert/UserTransaction.dart';
import 'package:nolido/widget/Composant.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

class Transaction extends StatefulWidget {
  String? fullName;
  String password = "";
  String account = "";


   Transaction({Key? key}) : super(key: key){
     getSharedPrefs();
   }



  Future<Null> getSharedPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    this.fullName = prefs.getString("fullName");
    this.password = prefs.getString("password")!;
    this.account = prefs.getString("account")!;
  }

  @override
  _TransactionState createState() => _TransactionState();
}

class _TransactionState extends State<Transaction> {
  late Future<List<UserTransaction>> futureData;
  Future refreshList() async {
    await Future.delayed(Duration(seconds: 2));
  }

  bool _loading = true;

  Future<Null> getSharedPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    widget.fullName = prefs.getString("fullName");
    widget.password = prefs.getString("password")!;
    widget.account = prefs.getString("account")!;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      showProgress();
      futureData = connexionToken(widget.account, widget.password);
      if (futureData==null) {
        futureData=[] as Future<List<UserTransaction>>;

        Timer(Duration(seconds: 2), hideProgress);
      }
      Timer(Duration(seconds: 2), hideProgress);
    });
  }

  final formatCurrency = new NumberFormat("#,##0.00");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.fromLTRB(0, 60, 0, 0),
                ),
                Column(
                  children: [
                    Container(
                      alignment: Alignment.topLeft,
                      child: Column(
                        children: [
                          Text(
                            "Mes transactions",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                fontSize: 25,
                                color: Colors.black54,
                                fontWeight: FontWeight.bold),
                          ),

                          RefreshIndicator(
                                  child: FutureBuilder<List<UserTransaction>>(
                                      future: futureData,
                                      builder: (context, snapshot) {
                                        if (snapshot.hasData) {
                                          List<UserTransaction>? data = snapshot.data;
                                          return Container(
                                            height: MediaQuery.of(context).size.height,
                                            color: Colors.white,
                                            child: ListView.builder(
                                                itemCount: data?.length,
                                                itemBuilder:
                                                    (BuildContext context, int index) {
                                                  return Container(
                                                    height: 85,
                                                    color: Colors.white,
                                                    child: Center(

                                                      child: TransactionRecentes(
                                                        data![index].amount,
                                                        data![index].receiver,
                                                        data![index].operator,
                                                        data![index].sendingCurrency,
                                                        data![index].transferId,
                                                        widget.account,
                                                        widget.password,
                                                      ),
                                                    ),
                                                  );
                                                }),
                                          );
                                        } else if (!snapshot.hasData){
                                          return Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            crossAxisAlignment: CrossAxisAlignment.end,
                                            children: [
                                              Center(child: Image.asset('images/noData.gif',height: 350,)),
                                            ],
                                          );
                                        }else if (snapshot.hasError) {
                                          return Image.asset('images/noInternet.gif');
                                        }
                                        return CircularProgressIndicator();
                                      }),
                              onRefresh: refreshList),

                          SizedBox(height: 85,)
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          new Offstage(
            offstage: _loading,
            child: _progressWidget(),
          ),
        ],
      ),
    );
  }

  Widget TransactionRecentes(
      double montant, String beneficiaire, String operateur, String sendingCurrency, String transferId, String account, String password) {
    return Container(

      child: Card(
        color: Colors.white,
        elevation: 2,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        child: Container(
          margin: EdgeInsets.fromLTRB(8, 0, 0, 2),
          width: MediaQuery.of(context).size.width / 1.1,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
          ),
          child: ElevatedButton(
            onPressed: (){
              connexionTokenDetail(transferId, account, password);
            },
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                      child: Text(
                        'Transféré de ',
                        style: TextStyle(
                            fontSize: 15,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                      child: Text(
                        '$sendingCurrency '+formatCurrency.format(montant),
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 15,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(25, 25, 10, 0),
                      child: Text(
                        '$operateur',
                        style: TextStyle(
                            fontSize: 15,
                            color: Colors.black54,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(10, 15, 10, 0),
                      child: SingleChildScrollView(
                        child: Text(
                          ''+beneficiaire.toString().substring(0,3)+'...',
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontSize: 15,
                              color: Colors.black54,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _progressWidget() {
    return new Center(
      child: new Stack(
        children: <Widget>[
          new Container(
            color: Colors.black45,
          ),
          Center(
            child: new SpinKitSpinningLines(
              color: Colors.orangeAccent,
              size: 100,
              lineWidth: 5,
            ),
          )
        ],
      ),
    );
  }

  //Navigator.of(context)
  //  .push(MaterialPageRoute(builder: (context) => LoadingStyle()));

  void showProgress() {
    if (mounted) {
      setState(() {
        print(">>>>>> showProgress >>> _loading : ${_loading}");

        _loading = false;

        print(">>>>>> showProgress >>> _loading : ${_loading}");
      });
    }
  }

  void hideProgress() {
    if (mounted) {
      setState(() {
        print(">>>>>> hideProgress >>> ");
        _loading = true;
      });
    }
  }

  //Fonction permettant de se connecter a l'application
  Future<List<UserTransaction>> connexionToken(
      String userLogin,
      String password,
      ) async {
    showProgress();
    String url = NolidoConstants.KEYCLOACK_URL;
    // 'https://www.qibisoft.com:8443/auth/realms/nolido/protocol/openid-connect/token';

    print(userLogin);

    Map<String, String> headers = {
      'content-type': 'application/x-www-form-urlencoded',
      'cache-control': 'no-cache',
    };

    Map<String, String> data = {
      'username': userLogin,
      'password': password,
      'grant_type': 'password',
      'client_id': 'nolido-mobile-application'
    };

    Uri uri = Uri.parse(url);
    http.Response response = await http.post(uri, headers: headers, body: data);
    // print(response.statusCode);
    hideProgress();
    if (response.statusCode == 200) {
      showProgress();
      Map<String, dynamic> token = json.decode(response.body);
      String accessToken = token['access_token'];
      print("Access token " + accessToken);
      print("userLogin " + userLogin);
      //Quand tous va bien et lance la fen principale
      print("Authentification succes");

      return fechListTrans(userLogin, password, accessToken);
    } else if (response.statusCode == 401) {
//Une erreur lors de la connexion
      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "login ou mot de passe incorrect",
          duration: Toast.lengthLong, gravity: Toast.bottom);
      return throw Exception('Connexion non autorisée');
    } else if (response.statusCode == 400) {
      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "Ce compte a deja été crée",
          duration: Toast.lengthLong, gravity: Toast.bottom);

      return throw Exception('La vérification par email est requise');
    } else {
      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "Le serveur rencontre une erreur",
          duration: Toast.lengthLong, gravity: Toast.bottom);

      throw Exception('Le serveur rencontre une erreur');
    }
  }

  Future<List<UserTransaction>> fechListTrans(
      userLogin,
      password,
      accessToken,
      ) async {
    showProgress();
    String url =
        NolidoConstants.SERVER_URL + NolidoConstants.TRANSFER_TRANSFERS_URL;
    //http://192.168.1.202:8080/v1/transfer/transfers

    print("accessToken $accessToken");
    try {
      Uri uri1 = Uri.parse(url);
      final response1 = await http.post(uri1,
          headers: {
            'Content-Type': 'application/json',
            //'cache-control': 'no-cache',
            //'Authorization': 'Bearer $accessToken',
          },
          body: jsonEncode(<String, dynamic>{
            "account": userLogin,
          }));
      hideProgress();

      if (response1.statusCode == 200) {
        List jsonResponse = json.decode(response1.body);
        return jsonResponse
            .map((data) => new UserTransaction.fromJson(data))
            .toList();
      } else if (response1.statusCode == 401) {
        print(userLogin);
        print(password);

        //Toast Message
        ToastContext().init(context);
        Composant().showToast("false", "login ou mot de passe incorrect",
            duration: Toast.lengthLong, gravity: Toast.bottom);

        return throw Exception('Connexion non autorisée');
      } else if (response1.statusCode == 400) {
        //Toast Message
        ToastContext().init(context);
        Composant().showToast("false", "Ce compte a deja été crée",
            duration: Toast.lengthLong, gravity: Toast.bottom);
        return throw Exception('La vérification par email est requise');
      } else {
        //Toast Message
        ToastContext().init(context);
        Composant().showToast("false", "Le serveur rencontre une erreur",
            duration: Toast.lengthLong, gravity: Toast.bottom);

        throw Exception('Le serveur rencontre une erreur');
      }
    } catch (e) {
      //  Map<String, dynamic> error = json.decode(response1!.body);
      // String errorMessage = error['message'];
      // print(errorMessage);

      /*  ToastContext().init(context);
      Composant().showToast("false", "Erreur",
          duration: Toast.lengthLong, gravity: Toast.bottom);*/
      throw Exception('Le serveur rencontre une erreur');
    }
  }


  //Fonction permettant de se connecter a l'application
  Future<dynamic> connexionTokenDetail(
      String transfertCd,
      String userLogin,
      String password,
      ) async {
    showProgress();
    String url = NolidoConstants.KEYCLOACK_URL;
    // 'https://www.qibisoft.com:8443/auth/realms/nolido/protocol/openid-connect/token';

    print(userLogin);

    Map<String, String> headers = {
      'content-type': 'application/x-www-form-urlencoded',
      'cache-control': 'no-cache',
    };

    Map<String, String> data = {
      'username': userLogin,
      'password': password,
      'grant_type': 'password',
      'client_id': 'nolido-mobile-application'
    };

    Uri uri = Uri.parse(url);
    http.Response response = await http.post(uri, headers: headers, body: data);
    // print(response.statusCode);
    hideProgress();
    if (response.statusCode == 200) {
      showProgress();
      Map<String, dynamic> token = json.decode(response.body);
      String accessToken = token['access_token'];
      print("Access token " + accessToken);
      print("userLogin " + userLogin);
      //Quand tous va bien et lance la fen principale
      print("Authentification succes");

      fechDetailTrans(transfertCd, accessToken);
    } else if (response.statusCode == 401) {
//Une erreur lors de la connexion
      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "login ou mot de passe incorrect",
          duration: Toast.lengthLong, gravity: Toast.bottom);
      return throw Exception('Connexion non autorisée');
    } else if (response.statusCode == 400) {
      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "Ce compte a deja été crée",
          duration: Toast.lengthLong, gravity: Toast.bottom);

      return throw Exception('La vérification par email est requise');
    } else {
      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "Le serveur rencontre une erreur",
          duration: Toast.lengthLong, gravity: Toast.bottom);

      throw Exception('Le serveur rencontre une erreur');
    }
  }

  Future<dynamic> fechDetailTrans(
      String transfertCd,
      String accessToken,
      ) async {
    showProgress();
    String url =
        NolidoConstants.SERVER_URL + NolidoConstants.TRANSFER_DISPLAY_URL;
    //'http://192.168.1.202:8080/v1/transfer/display';

    print("accessToken $accessToken");
    try {
      Uri uri1 = Uri.parse(url);
      final response1 = await http.post(uri1,
          headers: {
            'Content-Type': 'application/json',
           // 'cache-control': 'no-cache',
           // 'Authorization': 'Bearer $accessToken',
          },
          body: jsonEncode(<String, String>{
            "transfertCd": transfertCd,
          }));
      hideProgress();

      print("accessToken  " + accessToken);
      print("Numero transfert  " + transfertCd);

      if (response1.statusCode == 200) {
        Map<dynamic, dynamic> transInfo = json.decode(response1.body);

        print(transInfo);
        print('ici');

        print(transInfo['transfertDate']);
        print(transInfo['transfertMethod']);
        print(transInfo['targetCountryIndicative']);
        print(transInfo['countrySourceCd']);
        print(transInfo['sendingAmount']);
        print(transInfo['rate']);
        print(transInfo['receiveAmount']);
        print(transInfo['feeAmount']);
        print(transInfo['dueAmount']);
        print(transInfo['sendCurrency']);
        print(transInfo['receiveCurrency']);
        print(transInfo['receivePhone']);
        print(transInfo['operator']);
        print(transInfo['targetCountry']);
        print(transInfo['receiver']);

        Navigator.push(context, MaterialPageRoute(builder: (context) {
          return DetailTransfert(
            transInfo['transfertDate'],
            transInfo['transfertMethod'],
            transInfo['countrySourceCd'],
            transInfo['sendingAmount'],
            transInfo['rate'],
            transInfo['receiveAmount'],
            transInfo['feeAmount'],
            transInfo['dueAmount'],
            transInfo['sendCurrency'],
            transInfo['receiveCurrency'],
            transInfo['receivePhone'],
            transInfo['operator'],
            transInfo['targetCountry'],
            transInfo['receiver'],
          );
        }));
      } else if (response1.statusCode == 401) {
        //Toast Message
        ToastContext().init(context);
        Composant().showToast("false", "login ou mot de passe incorrect",
            duration: Toast.lengthLong, gravity: Toast.bottom);

        return throw Exception('Connexion non autorisée');
      } else if (response1.statusCode == 400) {
        //Toast Message
        ToastContext().init(context);
        Composant().showToast("false", "Ce compte a deja été crée",
            duration: Toast.lengthLong, gravity: Toast.bottom);
        return throw Exception('La vérification par email est requise');
      } else {
        //Toast Message
        ToastContext().init(context);
        Composant().showToast("false", "Le serveur rencontre une erreur",
            duration: Toast.lengthLong, gravity: Toast.bottom);

        throw Exception('Le serveur rencontre une erreur');
      }
    } catch (e) {
      //  Map<String, dynamic> error = json.decode(response1!.body);
      // String errorMessage = error['message'];
      // print(errorMessage);

      /*  ToastContext().init(context);
      Composant().showToast("false", "Erreur",
          duration: Toast.lengthLong, gravity: Toast.bottom);*/
      throw Exception('Le serveur rencontre une erreur');
    }
  }
}
