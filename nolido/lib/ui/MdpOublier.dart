import 'package:flutter/material.dart';
import 'package:nolido/ui/Add.dart';
import 'package:nolido/widget/Composant.dart';
import 'package:toast/toast.dart';

class MdpOublier extends StatefulWidget {
  const MdpOublier({Key? key}) : super(key: key);

  @override
  _MdpOublierState createState() => _MdpOublierState();
}

class _MdpOublierState extends State<MdpOublier> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Row(
              children: [
                Container(
                  padding: EdgeInsets.fromLTRB(0, 35, 0, 0),
                  child: IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: Icon(Icons.arrow_back_ios_outlined, color: Colors.black54,)),
                )
              ],
            ),
            Column(
              children: [
                Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.fromLTRB(20, 10, 0, 0),
                  child: Column(
                    children: [
                      Text(
                        "Mot de passe",
                        style: TextStyle(fontSize: 25, color: Colors.black54,fontWeight: FontWeight.bold),
                      ),

                    ],
                  ),
                ),

                Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                  child: Column(
                    children: [
                      Text(
                        "oublié",
                        style: TextStyle(fontSize: 25, color: Colors.black54,fontWeight: FontWeight.bold),
                      ),

                    ],
                  ),
                ),
              ],
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
              child: Column(
                children: [
                  SizedBox(height: 25,),
                  Text(
                    "Entrez le numéro de téléphone que vous utilisez pour créer votre compte",
                    style: TextStyle(fontSize: 15, color: Colors.black54),
                  ),

                ],
              ),
            ),


            Container(
              padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
              child: Composant().phoneTF2("Numéro de téléphone"),
            ),

            Container(
              width: MediaQuery.of(context).size.width,
              height: 85,
              padding: EdgeInsets.fromLTRB(20, 30, 20, 10),
              child: ElevatedButton(
                onPressed: () {
                  ToastContext().init(context);
                  Composant().showToast("false", "Veuiller entrez un numéro de téléphone!",duration: Toast.lengthLong,gravity: Toast.bottom);
                },
                child: Text(
                  "Continuer",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
            ),
            
          ],
        ),
      ),
    );
  }
}
