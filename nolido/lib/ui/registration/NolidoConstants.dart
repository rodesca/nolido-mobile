class NolidoConstants{

  static final KEYCLOACK_URL='http://www.qibisoft.com:8080/auth/realms/nolido/protocol/openid-connect/token';

  static final TOUCH_PAY_URL='https://touchpay.qutouch.com/touchpay/script/prod_touchpay-0.0.1.js';

  static final TOUCH_PAY_COMPLET_URL='http://touchpay.qutouch.com/touchpay/script/prod_touchpay-0.0.1.js?payment_mode=INTOUCH_SERVICE_CODE&paid_sum=22200.0&paid_amount=22200.0&payment_token=1565251468191&payment_status=200&command_number=14526&payment_validation_date=1565251499748';

  //static final SERVER_URL='http://192.168.0.158:8080/v1/';
  static final SERVER_URL='http://www.qibisoft.com:8080/nolido/v1/';

  static final REGISTRATION_URL='registration/';

  static final REGISTRATION_CONFIRME_URL='registration/confirm';

  static final REGISTRATION_VALTOKEN_URL='registration/valtoken';

  static final TRANSFER_TRANSFER_URL='transfer/transfer';

  static final TRANSFER_RECENTS_URL='transfer/recents';

  static final TRANSFER_DISPLAY_URL='transfer/display';

  static final REGISTRATION_TOKEN_URL='registration/token';

  static final REGISTRATION_PHONE_URL='registration/phone';

  static final TRANSFER_REGISTRATION_CHANGE_URL='registration/change';

  static final TRANSFER_TRANSFERS_URL='transfer/transfers';

  static final INFO_URL='info';

  static final TRANSFERT_SIMULATE_URL='transfer/simulate';

  static final ADD_PHONE_METHOD='phone';

  static final REGISTRATION_URL_CONFIRM='confirm';

  static final CONTENT_TYPE=  'Content-Type';

  static final JSON_CONTENT_TYPE=  'application/json';

  static final NO_CACHE=  'cache-control';

  static final ACCEPT=  'Accept';

  static final JSON=  'application/json';

  static final AUTHORIZATION=  'Authorization';


  static final Map<String, String> HEADERS = {
    'content-type': 'application/x-www-form-urlencoded',
    'cache-control': 'no-cache',
  };

  static final Map<String, String> BODY_KEYCLOAK = {
    'username': 'xxxxxx',
    'password': 'cccccc',
    'grant_type': 'password',
    'client_id': 'nolido-mobile-application'
  };

}