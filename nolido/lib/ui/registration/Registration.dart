import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:nolido/ui/RegistrationCode.dart';
import 'package:nolido/ui/registration/NolidoConstants.dart';
import 'package:nolido/ui/security/AuthRepository.dart';
import 'package:nolido/widget/Composant.dart';
import 'package:nolido/widget/Loading.dart';
import 'package:toast/toast.dart';

/// This class permit to create user (Business or Physical )
class Registration {
  String? _phoneNumber;
  String? _password;
  String? _fullName;
  String? _raisonSociale;
  String? _RCCM;
  String? _countryIndication;
  String? _userType;
  String? _login;
  TextEditingController txtActivationCode = new TextEditingController();

  Registration(
      this._userType,
      this._countryIndication,
      this._phoneNumber,
      this._fullName,
      this._login,
      this._password,
      this._raisonSociale,
      this._RCCM);

  Future registration(BuildContext context) async {
    createUsr(context).then((res) async {
      Map<String, dynamic> message = await json.decode(res.body);
      var error = message['message'];

      if (error != null && error.toString().trim().isNotEmpty) {
        List<int> bytes = error.toString().codeUnits;

        ToastContext().init(context);
        Composant().showToast("false", utf8.decode(bytes),
            duration: Toast.lengthLong, gravity: Toast.bottom);
      } else {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => RegistrationCode(_login, _password, _countryIndication, _phoneNumber)));
      }
    });
  }

  Future registrationRenvoyer(BuildContext context) async {
    createUsr(context).then((res) async {
      Map<String, dynamic> message = json.decode(res.body);
      var error = message['message'];

      if (error != null && error.toString().trim().isNotEmpty) {
        List<int> bytes = error.toString().codeUnits;

        ToastContext().init(context);
        Composant().showToast("false", utf8.decode(bytes),
            duration: Toast.lengthLong, gravity: Toast.bottom);
      }
    });
  }

  Future openDialog(BuildContext context) async {
    return await showDialog(
      barrierDismissible: false,
      context: context,
      builder: (ctx) => AlertDialog(
        title: Text("Veuillez Entrer votre clé d'activation"),
        content: Container(
          child: TextField(
            controller: txtActivationCode,
            keyboardType: TextInputType.number,
            obscureText: false,
            decoration: InputDecoration(
              labelText: "Entrer votre code d'activation",
            ),
          ),
        ),
        actions: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              ElevatedButton(
                onPressed: () {
                  if (txtActivationCode.text != null ||
                      !txtActivationCode.text.isEmpty) {
                    registrationRenvoyer(context);
                  } else {
                    ToastContext().init(context);
                    Composant().showToast(
                        "false", "Entrer votre code d'activation",
                        duration: Toast.lengthLong, gravity: Toast.bottom);
                  }
                },
                child: Text("Renvoyer le code"),
              ),
              ElevatedButton(
                onPressed: () {
                  if (txtActivationCode.text != null ||
                      !txtActivationCode.text.isEmpty) {
                    print('txtActivationCode ' + txtActivationCode.text);
                    print('login $_login');
                    print('password $_password');

                    AuthRepository().confirmationCodePin(
                        context, _login!, _password!, txtActivationCode.text,_countryIndication!, _phoneNumber!);
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => Loading()));
                    print("attente de confirmation");
                  } else {
                    ToastContext().init(context);
                    Composant().showToast(
                        "false", "Entrer votre code d'activation",
                        duration: Toast.lengthLong, gravity: Toast.bottom);
                  }
                },
                child: Text("Confimer"),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Future<dynamic> createUsr(BuildContext context) async {
    Map<String, String> headers = {
      NolidoConstants.CONTENT_TYPE: NolidoConstants.JSON_CONTENT_TYPE,
      NolidoConstants.NO_CACHE: NolidoConstants.NO_CACHE,
      NolidoConstants.ACCEPT: NolidoConstants.JSON,
      //NolidoConstants.AUTHORIZATION: 'Bearer $response'
    };
    Map<String, String> requestBody = {
      "phone": this._phoneNumber.toString(),
      "login": this._login.toString(),
      "countryCode": this._countryIndication.toString(),
      "password": this._password.toString(),
      "userType": this._userType.toString(),
      "fullName": this._fullName.toString()
    };
    try {
      Uri uri = Uri.parse(NolidoConstants.SERVER_URL +
          NolidoConstants.REGISTRATION_URL +
          NolidoConstants.ADD_PHONE_METHOD);
      http.Response responseQuery =
          await http.post(uri, headers: headers, body: jsonEncode(requestBody));
      if (responseQuery.statusCode == 200 || responseQuery.statusCode == 201) {
        return responseQuery;
      }
      if (responseQuery.statusCode == 400) {
        return responseQuery;
      }
      if (responseQuery.statusCode == 401) {
        return responseQuery;
      }
      if (responseQuery.statusCode == 500) {
        return responseQuery;
      }
    } on SocketException catch (e) {
      ToastContext().init(context);
      Composant().showToast(
          "false", "Véuillez verifier votre connexion internet",
          duration: Toast.lengthLong, gravity: Toast.bottom);
    } on Exception {
      ToastContext().init(context);
      Composant().showToast(
          "false", "Une erreur innatendue est survenue, veuillez réessayer",
          duration: Toast.lengthLong, gravity: Toast.bottom);
    }
  }
}
