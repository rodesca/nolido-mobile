
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:nolido/widget/Composant.dart';
import 'package:nolido/widget/Loading.dart';
import 'package:toast/toast.dart';

import 'NolidoConstants.dart';
import 'package:http/http.dart' as http;

class Authentificaton {
  String tokenGlobal = "";
  static Authentificaton? _instance;

  Authentificaton (){

  }

  static getInstance (){
     if (_instance==null) _instance=new  Authentificaton ();
     return _instance;
  }

  Future<dynamic> signin(BuildContext context) async {
    String url = NolidoConstants.KEYCLOACK_URL;
    Map<String, String> headers = NolidoConstants.HEADERS;
    Map<String, String> data =NolidoConstants.BODY_KEYCLOAK;
    Uri uri = Uri.parse(url);
    try{
      http.Response response = await http.post(uri, headers: headers, body: data);
      if (response.statusCode == 200) {
        Map<String, dynamic> token = json.decode(response.body);
        String accessToken = token['access_token'];
        tokenGlobal = accessToken;
        print(tokenGlobal);
        print("-------------------------------------");
        print(tokenGlobal);
         return accessToken;
      }
    }on SocketException catch (e){
      ToastContext().init(context);
      Composant().showToast("false", "Véuillez verifier votre connexion internet", duration: Toast.lengthLong, gravity: Toast.bottom);
    }on Exception{
      ToastContext().init(context);
      Composant().showToast("false", "Une erreur innatendue est survenue, veuillez réessayer", duration: Toast.lengthLong, gravity: Toast.bottom);
    }
  }



  Future registrationConfirme(CodeConfirmation, login) async {
    String url1 = NolidoConstants.SERVER_URL +
        NolidoConstants.REGISTRATION_URL +
        NolidoConstants.REGISTRATION_URL_CONFIRM;

    Map<String, String> headers = {
      "Content-Type": "application/json",
      'cache-control': 'no-cache',
      'Accept': 'application/json',
      'Authorization': 'Bearer $tokenGlobal'
    };

    Uri uri1 = Uri.parse(url1);
    http.Response response = await http
        .post(uri1,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer $tokenGlobal',
        },
        body: jsonEncode(<String, String>{
          "token": CodeConfirmation,
          "account": login,
        }))
        .whenComplete(() => print("operation ok"));
    print(uri1);
    print(response.statusCode);
    print(response.body);


    if (response.statusCode == 200) {
      return throw Exception('Connexion succes');
    } else {
      if (response.statusCode == 401) {
        return throw Exception('Connexion succes');
      }else{
        if (response.statusCode == 401) {
          return throw Exception('Compte deja créer');
        }

      }
    }
  }
}