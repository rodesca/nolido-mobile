class FindFreeTransaction{
  late String operator;
  late String sendingCurrency;
  late double amount;
  late String receiver;

  FindFreeTransaction(this.operator, this.sendingCurrency, this.amount, this.receiver);

  FindFreeTransaction.fromJson(Map<String, dynamic> json){
    operator = json['operator'];
    sendingCurrency = json['sendingCurrency'];
    amount = json[amount];
    receiver = json['receiver'];
  }

  Map<String, dynamic> toJson(){
    return {
      'operator' : operator,
      'sendingCurrency' : sendingCurrency,
      'amount' : amount,
      'receiver' : receiver,
    };
  }

}