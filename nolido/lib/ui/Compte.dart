import 'dart:convert';
import 'dart:io';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:nolido/ui/Connexion.dart';
import 'package:nolido/ui/registration/NolidoConstants.dart';
import 'package:nolido/widget/Composant.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

class Compte extends StatefulWidget {
  String full = "";
  String countryInd = "";
  String userLogin = "";
  String phone = "";
  String password = "";
  String VarTel = "";
  String inPays = "";

  Compte(this.full, this.countryInd, this.userLogin, this.phone, this.password,
      {Key? key})
      : super(key: key);

  @override
  _CompteState createState() => _CompteState(
      this.full, this.countryInd, this.userLogin, this.phone, this.password);
}

class _CompteState extends State<Compte> {
  _CompteState(String? fullName, String account, String countryIndicative,
      String phone, String password);

  TextEditingController controllervalueResponse = new TextEditingController();
  TextEditingController idCountry = new TextEditingController();
  bool _loading = true;
  String VarTel = "";
  String inPays = "";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                Row(
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(0, 35, 0, 0),
                      child: IconButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          icon: Icon(
                            Icons.arrow_back_ios_outlined,
                            color: Colors.black54,
                          )),
                    )
                  ],
                ),
                Column(
                  children: [
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                      child: Column(
                        children: [
                          Text(
                            "Compte",
                            style: TextStyle(
                                fontSize: 25,
                                color: Colors.black54,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    PropetiesCompte(widget.userLogin, 0),
                    SizedBox(
                      height: 10,
                    ),
                    PropetiesCompte(
                        controllervalueResponse.text.isEmpty
                            ? widget.full
                            : controllervalueResponse.text,
                        1),
                    SizedBox(
                      height: 10,
                    ),
                    PropetiesCompte(widget.countryInd + " " + widget.phone, 2),
                  ],
                ),
              ],
            ),
          ),
          new Offstage(
            offstage: _loading,
            child: _progressWidget(),
          ),
        ],
      ),
    );
  }

  Widget _progressWidget() {
    return new Center(
      child: new Stack(
        children: <Widget>[
          new Container(
            color: Colors.black45,
          ),
          Center(
            child: new SpinKitSpinningLines(
              color: Colors.orangeAccent,
              size: 100,
              lineWidth: 5,
            ),
          )
        ],
      ),
    );
  }

  //Navigator.of(context)
  //  .push(MaterialPageRoute(builder: (context) => LoadingStyle()));

  void showProgress() {
    if (mounted) {
      setState(() {
        print(">>>>>> showProgress >>> _loading : ${_loading}");

        _loading = false;

        print(">>>>>> showProgress >>> _loading : ${_loading}");
      });
    }
  }

  void hideProgress() {
    if (mounted) {
      setState(() {
        print(">>>>>> hideProgress >>> ");
        _loading = true;
      });
    }
  }

  void change() {
    IntlPhoneField(
        initialValue: "",
        onChanged: (phone) {
          VarTel = "";
          print(inPays);
        });
  }

  Widget phoneTF2(String label) => IntlPhoneField(
        controller: idCountry,
        disableLengthCheck: true,
        initialValue: "",
        onCountryChanged: (value) {
          return change();
        },
        decoration: InputDecoration(
          focusColor: Colors.orange,
          labelText: label,
        ),
        initialCountryCode: 'SN',
        invalidNumberMessage: "Numéro invalide",
        onChanged: (phone) {
          setState(() {
            VarTel = phone.number;
            inPays = phone.countryCode;
          });
        },
      );

  Future openDialog(BuildContext context, String name, String contryInd,
      String valeur, String UserLogin, int change) async {
    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (ctx) => AlertDialog(
        title: Text(
          "Veuillez modifier $name",
          style: TextStyle(fontSize: 20),
        ),
        content: Container(
          child: change == 1
              ? TextField(
                  controller: controllervalueResponse,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    labelText: "$contryInd$valeur",
                    prefixText: "$contryInd",
                  ),
                )
              : phoneTF2("Numéro de téléphone"),
        ),
        actions: <Widget>[
          Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  ElevatedButton(
                    onPressed: () {

                      Navigator.of(context).pop();
                      controllervalueResponse.clear();
                    },
                    child: Text("Annuler"),
                  ),
                  ElevatedButton(
                    onPressed: () {

                      setState(() {
                        if (change == 1) {
                          //change == 1 Changement du nom et prenom
                          if (controllervalueResponse.text.trim().isNotEmpty) {
                            print(controllervalueResponse.text.trim());

                            connexionTokenDetail(controllervalueResponse.text.trim(),
                                widget.userLogin, widget.password);

                            Navigator.pop(context);
                          } else {
                            //controllervalueResponse Vide
                            ToastContext().init(context);
                            Composant().showToast(
                                "false", "Veuillez remplir ce champs !",
                                duration: Toast.lengthLong,
                                gravity: Toast.bottom);
                          }
                        } else {
                          var numeroTel = inPays + VarTel;

                          //change == 2 Changement du numero de telephone
                          if (numeroTel.isNotEmpty) {
                            print(numeroTel);

                            ConnexionToken(widget.userLogin, widget.password, widget.countryInd, widget.phone);

                          } else {
                            ToastContext().init(context);
                            Composant().showToast(
                                "false", "Veuillez remplir ce champs !",
                                duration: Toast.lengthLong,
                                gravity: Toast.bottom);
                          }
                        }
                      });
                    },
                    child: Text("Confimer"),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }


  Future openDialogCodeConfirmation(BuildContext context,String codeToken, String name, String contryInd,
      String phone, String UserLogin, String accessToken) async {
    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (ctx) => AlertDialog(
        title: Text(
          "Veuillez modifier $name",
          style: TextStyle(fontSize: 20),
        ),
        content: Container(
          child: change == 1
              ? TextField(
            controller: controllervalueResponse,
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
              labelText: "$contryInd$phone",
              prefixText: "$contryInd",
            ),
          )
              : phoneTF2("Numéro de téléphone"),
        ),
        actions: <Widget>[
          Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  ElevatedButton(
                    onPressed: () {
                      controllervalueResponse.clear();
                      Navigator.of(context).pop();
                    },
                    child: Text("Annuler"),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      setState(() {
                        if(controllervalueResponse.text.trim() ==  codeToken){

                          ConfirmeCode(codeToken, UserLogin, contryInd+phone, accessToken);

                        }else{

                          ToastContext().init(context);
                          Composant().showToast(
                              "false", "Code de confirmation incorrect",
                              duration: Toast.lengthLong,
                              gravity: Toast.bottom);
                          controllervalueResponse.clear();
                        }



                      });
                    },
                    child: Text("Confimer"),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }


  Widget PropetiesCompte(String propeties, int change) {
    return Card(
      color: Colors.white,
      elevation: 10,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      child: Container(
        margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
        width: MediaQuery.of(context).size.width / 1.1,
        height: 65,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
        ),
        child: ElevatedButton(
          onPressed: () {
            if (change == 1) {
              // connexionTokenDetail(widget.full, widget.userLogin, password)
              openDialog(
                  context, "Nom complet", "", widget.full, widget.userLogin, 1);
            } else {
              if (change == 2) {
                openDialog(context, "Numéro de téléphone", widget.countryInd,
                    widget.phone, "", 2);
              } else {}
            }
          },
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding: EdgeInsets.fromLTRB(10, 20, 10, 0),
                    child: Text(
                      '$propeties',
                      style: TextStyle(
                          fontSize: 15,
                          color: Colors.black54,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(10, 14, 10, 0),
                    child: Row(
                      children: [
                        change == 0
                            ? Text("")
                            : Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.black54,
                              ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }


  //Fonction permettant de ce connecter a l'application
  Future<dynamic> ConnexionToken(String userLogin, String password, String phoneIndicative, String phone) async {
    showProgress();

    String url = NolidoConstants.KEYCLOACK_URL;

    Map<String, String> headers = {
      'content-type': 'application/x-www-form-urlencoded',
      'cache-control': 'no-cache',
    };

    Map<String, String> data = {
      'username': userLogin,
      'password': password,
      'grant_type': 'password',
      'client_id': 'nolido-mobile-application'
    };

    Uri uri = Uri.parse(url);
    http.Response response = await http.post(uri, headers: headers, body: data);
    // print(response.statusCode);
    hideProgress();
    try{


    if (response.statusCode == 200) {
      showProgress();
      Map<String, dynamic> token = json.decode(response.body);
      String accessToken = token['access_token'];
      print("Access token " + accessToken);
      print("userLogin " + userLogin);
//Quand tous va bien et lance la fen principale
      print("Authentification succes");

      genetaionCodeConfirmation(userLogin, phoneIndicative, phone, accessToken);

      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString("password", password);
    } else if (response.statusCode == 401) {
//Une erreur lors de la connexion
      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "login ou mot de passe incorrect",
          duration: Toast.lengthLong, gravity: Toast.bottom);
      return throw Exception('Connexion non autorisée');
    } else if (response.statusCode == 400) {
      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "Ce compte a deja été crée",
          duration: Toast.lengthLong, gravity: Toast.bottom);

      return throw Exception('La vérification par email est requise');
    } else {
      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "Le serveur rencontre une erreur",
          duration: Toast.lengthLong, gravity: Toast.bottom);

      throw Exception('Le serveur rencontre une erreur');
    }
    }on SocketException catch (e){
      ToastContext().init(context);
      Composant().showToast("false", "Véuillez verifier votre connexion internet", duration: Toast.lengthLong, gravity: Toast.bottom);
    }on Exception{
      ToastContext().init(context);
      Composant().showToast("false", "Une erreur innatendue est survenue, veuillez réessayer", duration: Toast.lengthLong, gravity: Toast.bottom);
    }
  }



  Future<dynamic> genetaionCodeConfirmation(
      String account,
      String phoneIndicative,
      String phone,
      String accessToken,
      ) async {
    showProgress();
    String url =
        NolidoConstants.SERVER_URL + NolidoConstants.REGISTRATION_TOKEN_URL;
    //'http://192.168.0.158:8080/v1/registration/token'

    print("accessToken $accessToken");
    try {
      Uri uri1 = Uri.parse(url);
      final response1 = await http.post(uri1,
          headers: {
            'Content-Type': 'application/json',
            'cache-control': 'no-cache',
            'Authorization': 'Bearer $accessToken',
          },
          body: jsonEncode(<String, String>{
            "account" : account,
            "phoneIndicative": phoneIndicative,
            "phone": phone
          }));
      hideProgress();

      print("accessToken  " + accessToken);

      if (response1.statusCode == 200) {
        Map<dynamic, dynamic> tokenInfo = json.decode(response1.body);

        print(tokenInfo);
        print('ici');

        print(tokenInfo['token']);
        print(tokenInfo['account']);
        print(tokenInfo['phoneNumber']);
        print(tokenInfo['countryIndicative']);

        var confirmeToken = tokenInfo['token'];

        openDialogCodeConfirmation(context,confirmeToken, "Numéro de téléphone", tokenInfo['countryIndicative'], phone, tokenInfo['account'], accessToken);

      } else if (response1.statusCode == 401) {
        //Toast Message
        ToastContext().init(context);
        Composant().showToast("false", "login ou mot de passe incorrect",
            duration: Toast.lengthLong, gravity: Toast.bottom);

        return throw Exception('Connexion non autorisée');
      } else if (response1.statusCode == 400) {
        //Toast Message
        ToastContext().init(context);
        Composant().showToast("false", "Ce compte a deja été crée",
            duration: Toast.lengthLong, gravity: Toast.bottom);
        return throw Exception('La vérification par email est requise');
      } else {
        //Toast Message
        ToastContext().init(context);
        Composant().showToast("false", "Le serveur rencontre une erreur",
            duration: Toast.lengthLong, gravity: Toast.bottom);

        throw Exception('Le serveur rencontre une erreur');
      }
    } catch (e) {
      //  Map<String, dynamic> error = json.decode(response1!.body);
      // String errorMessage = error['message'];
      // print(errorMessage);

      /*  ToastContext().init(context);
      Composant().showToast("false", "Erreur",
          duration: Toast.lengthLong, gravity: Toast.bottom);*/
      throw Exception('Le serveur rencontre une erreur');
    }
  }


  Future ConfirmeCode(CodeConfirmation, account, phoneNumber, accessToken) async {
    String url1 = NolidoConstants.SERVER_URL +NolidoConstants.REGISTRATION_VALTOKEN_URL;
  //  http://192.168.0.158:8080/v1/registration/valtoken


    Uri uri1 = Uri.parse(url1);
    http.Response response = await http
        .post(uri1,
        headers: {
          'Content-Type': 'application/json',
          'cache-control': 'no-cache',
          'Authorization': 'Bearer $accessToken',
        },
        body: jsonEncode(<String, String>{
          "account" : account,
          "token": CodeConfirmation,
          "phoneNumber": phoneNumber,
        }))
        .whenComplete(() => print("operation ok"));
    print(uri1);
    print(response.statusCode);
    print(response.body);

    if (response.statusCode == 200) {

      ToastContext().init(context);
      Composant().showToast("true", "Opération éffectuée avec succès, veuillez vous connecter à nouveau.",
          duration: Toast.lengthLong, gravity: Toast.bottom);

      Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => const Connexion()));

      return throw Exception('Connexion non autorisée');
      return throw Exception('Connexion succes');
    } else {
      if (response.statusCode == 401) {
        return throw Exception('Connexion succes');
      }else{
        if (response.statusCode == 401) {
          return throw Exception('Compte deja créer');
        }

      }
    }
  }


  //Fonction permettant de se connecter a l'application
  Future<dynamic> connexionTokenDetail(
    String fullName,
    String userLogin,
    String password,
  ) async {
    showProgress();
    String url = NolidoConstants.KEYCLOACK_URL;
    // 'https://www.qibisoft.com:8443/auth/realms/nolido/protocol/openid-connect/token';

    print(userLogin);

    Map<String, String> headers = {
      'content-type': 'application/x-www-form-urlencoded',
      'cache-control': 'no-cache',
    };

    Map<String, String> data = {
      'username': userLogin,
      'password': password,
      'grant_type': 'password',
      'client_id': 'nolido-mobile-application'
    };

    Uri uri = Uri.parse(url);
    http.Response response = await http.post(uri, headers: headers, body: data);
    // print(response.statusCode);
    hideProgress();
    if (response.statusCode == 200) {
      showProgress();
      Map<String, dynamic> token = json.decode(response.body);
      String accessToken = token['access_token'];
      print("Access token " + accessToken);
      print("userLogin " + userLogin);
      //Quand tous va bien et lance la fen principale
      print("Authentification succes");

      UdpateName(accessToken, userLogin, fullName);
    } else if (response.statusCode == 401) {
//Une erreur lors de la connexion
      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "login ou mot de passe incorrect",
          duration: Toast.lengthLong, gravity: Toast.bottom);
      return throw Exception('Connexion non autorisée');
    } else if (response.statusCode == 400) {
      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "Ce compte a deja été crée",
          duration: Toast.lengthLong, gravity: Toast.bottom);

      return throw Exception('La vérification par email est requise');
    } else {
      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "Le serveur rencontre une erreur",
          duration: Toast.lengthLong, gravity: Toast.bottom);

      throw Exception('Le serveur rencontre une erreur');
    }
  }

  Future<dynamic> UdpateName(
    String accessToken,
    String userLogin,
    String fullName,
  ) async {
    showProgress();
    String url = NolidoConstants.SERVER_URL +
        NolidoConstants.TRANSFER_REGISTRATION_CHANGE_URL;
    //'http://192.168.0.158:8080/v1/registration/change';

    print("accessToken $accessToken");
    try {
      Uri uri1 = Uri.parse(url);
      final response1 = await http.post(uri1,
          headers: {
            'Content-Type': 'application/json',
            'cache-control': 'no-cache',
            'Authorization': 'Bearer $accessToken',
          },
          body: jsonEncode(<String, String>{
            "account": userLogin,
            "fullName": fullName,
          }));
      hideProgress();

      print("accessToken  " + accessToken);

      if (response1.statusCode == 200) {
        Map<dynamic, dynamic> transInfo = json.decode(response1.body);

        ToastContext().init(context);
        Composant().showToast("true", "Modification éffectuée, veuillez vous connecter à nouveau",
            duration: Toast.lengthLong, gravity: Toast.bottom);

        Navigator.pushReplacement<void, void>(
          context,
          MaterialPageRoute<void>(
            builder: (BuildContext context) => const Connexion(),
          ),
        );
      } else if (response1.statusCode == 401) {
        //Toast Message
        ToastContext().init(context);
        Composant().showToast("false", "login ou mot de passe incorrect",
            duration: Toast.lengthLong, gravity: Toast.bottom);

        return throw Exception('Connexion non autorisée');
      } else if (response1.statusCode == 400) {
        //Toast Message
        ToastContext().init(context);
        Composant().showToast("false", "Ce compte a deja été crée",
            duration: Toast.lengthLong, gravity: Toast.bottom);
        return throw Exception('La vérification par email est requise');
      } else {
        //Toast Message
        ToastContext().init(context);
        Composant().showToast("false", "Le serveur rencontre une erreur",
            duration: Toast.lengthLong, gravity: Toast.bottom);

        throw Exception('Le serveur rencontre une erreur');
      }
    } catch (e) {
      //  Map<String, dynamic> error = json.decode(response1!.body);
      // String errorMessage = error['message'];
      // print(errorMessage);

      /*  ToastContext().init(context);
      Composant().showToast("false", "Erreur",
          duration: Toast.lengthLong, gravity: Toast.bottom);*/
      throw Exception('Le serveur rencontre une erreur');
    }
  }
}
