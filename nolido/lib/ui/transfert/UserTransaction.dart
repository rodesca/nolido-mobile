import 'dart:core';

class UserTransaction {
  String transferId;

  String sendingCurrency;

  double amount;

  String operator;

  String receiver;

  UserTransaction({required this.transferId, required this.sendingCurrency, required this.amount,
    required this.operator, required this.receiver});

  factory UserTransaction.fromJson(Map<String, dynamic> j){
    return UserTransaction(
        transferId : j['transferId'],
        sendingCurrency : j['sendingCurrency'],
        amount : j['amount'],
        operator: j['operator'],
        receiver:j['receiver'],
    );
  }


  Map toMap(){
    return {
      'transferId':transferId,
      'sendingCurrency':sendingCurrency,
      'amount':amount,
      'operator':operator,
      'receiver':receiver,
    };
  }
}
