import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:webview_flutter/platform_interface.dart';
import 'package:webview_flutter/webview_flutter.dart';

//import 'package:webview_flutter/webview_flutter.dart';
class WebAppTouchPay extends StatefulWidget {
  const WebAppTouchPay({Key? key}) : super(key: key);

  @override
  _WebAppTouchPayState createState() => _WebAppTouchPayState();
}

class _WebAppTouchPayState extends State<WebAppTouchPay> {
  bool _loading = true;

  @override
  void initState() {
    // TODO: implement initState
    showProgress();
    Timer(Duration(seconds: 2), hideProgress);
    super.initState();
  }
  @override
  void dispose() {
    // TODO: implement disposes
    hideProgress();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            onPressed: (){
              Navigator.pop(context);
            }, icon: Icon(Icons.arrow_back_ios_rounded), color: Colors.white,),
        title: const Text("Nolido-Pay",style: TextStyle(color: Colors.white),),
      ),
      body: Stack(
        children: [
          webViewWidget(),
          new Offstage(
            offstage: _loading,
            child: _progressWidget(),
          ),
        ],

      )

    );

  }
  Widget _progressWidget() {
    return Center(
      child: Stack(
        children: <Widget>[
          Container(
            color: Colors.black45,
          ),
        const Center(
            child:  SpinKitSpinningLines(
              color: Colors.orangeAccent,
              size: 100,
              lineWidth: 5,
            ),
          )
        ],
      ),
    );
  }

//Navigator.of(context)
//  .push(MaterialPageRoute(builder: (context) => LoadingStyle()));

  void showProgress() {
    if (mounted) {
      setState(() {
        print(">>>>>> showProgress >>> _loading : ${_loading}");

        _loading = false;

        print(">>>>>> showProgress >>> _loading : ${_loading}");
      });
    }
  }

  void hideProgress() {
    if (mounted) {
      setState(() {
        print(">>>>>> hideProgress >>> ");
        _loading = true;
      });
    }
  }

   webViewWidget(){
    return const WebView(
        initialUrl: 'http://192.168.0.134:8080');
  }
/*
  webViewPlusWidget(){
    return const WebViewPlus(
      javascriptMode: JavascriptMode.unrestricted,
      onWebViewCreated: (controller) {
        controller.loadString(r"""
           <html lang="en">
            <body>hello world</body>
           </html>
      """);
      },
    );
  }
 */
}



