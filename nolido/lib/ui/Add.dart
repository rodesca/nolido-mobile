import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:nolido/ui/LoadingStyle.dart';
import 'package:nolido/ui/registration/NolidoConstants.dart';
import 'package:nolido/ui/registration/Registration.dart';
import 'package:nolido/widget/Composant.dart';
import 'package:nolido/widget/Loading.dart';
import 'package:toast/toast.dart';

class Add extends StatefulWidget {
  const Add({Key? key}) : super(key: key);

  @override
  _AddState createState() => _AddState();
}

class _AddState extends State<Add> {
  bool _loading = true;
  var pin;
  var VarTel;
  bool Visibiliter = false;
  var VarMdp;
  String RecTypeUser = "I";
  String dropdownValue = 'Individuel';
  String indicePays = "";
  String inPays = "";

  TextEditingController login = new TextEditingController();
  TextEditingController idCountry = new TextEditingController();
  TextEditingController txtPassword = new TextEditingController();
  TextEditingController txtPasswordConfirmation = new TextEditingController();
  TextEditingController controllerPin = new TextEditingController();
  TextEditingController txtPhoneNumber = new TextEditingController();
  TextEditingController txtFullName = new TextEditingController();
  TextEditingController RaisonSociale = new TextEditingController();
  TextEditingController RCCM = new TextEditingController();

  void initState() {
    // TODO: implement initState
    super.initState();
    //  ChampsVisible(Visibiliter);
    //  texteTF("controler", "labelText");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                Row(
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(0, 35, 0, 0),
                      child: IconButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          icon: Icon(
                            Icons.arrow_back_ios_outlined,
                            color: Colors.black54,
                          )),
                    )
                  ],
                ),
                Column(
                  children: [
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.fromLTRB(20, 10, 0, 0),
                      child: Column(
                        children: [
                          Text(
                            "S'enregistrer",
                            style: TextStyle(
                                fontSize: 25,
                                color: Colors.black54,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                ComboCkeck(context),
                SizedBox(
                  height: 10,
                ),
                VisibleEntreprise(Visibiliter),
                SizedBox(
                  height: 10,
                ),
                VisibleRCCM(Visibiliter),
                SizedBox(
                  height: 5,
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                  child: phoneTF2("Numéro de téléphone"),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
                  child: TextField(
                    autocorrect: true,
                    controller: txtFullName,
                    keyboardType: TextInputType.text,
                    obscureText: false,
                    decoration: InputDecoration(
                      labelText: 'Nom(s) & Prénom(s)',
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
                  child: TextField(
                    controller: login,
                    keyboardType: TextInputType.text,
                    obscureText: false,
                    decoration: InputDecoration(
                      labelText: 'Login',
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
                  child: TextField(
                    controller: txtPassword,
                    keyboardType: TextInputType.text,
                    obscureText: true,
                    decoration: InputDecoration(
                      labelText: 'Mot de passe',
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
                  child: TextField(
                    controller: txtPasswordConfirmation,
                    keyboardType: TextInputType.text,
                    obscureText: true,
                    decoration: InputDecoration(
                      labelText: 'Confirmer le mot de passe',
                    ),
                  ),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 25,
                      ),
                      Text(
                        "En appuyant sur <créer> vous acceptez les conditions d'utilisation et la politique de Nolido ",
                        style: TextStyle(fontSize: 15, color: Colors.black54),
                      ),
                    ],
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 85,
                  padding: EdgeInsets.fromLTRB(20, 30, 20, 10),
                  child: ElevatedButton(
                    onPressed: () {
                      setState(() {
                        showProgress();
                        _progressWidget();
                        Timer(Duration(seconds: 2), hideProgress);
                      });

                      if (inPays != "" &&
                          VarTel != "" &&
                          txtFullName.text != "" &&
                          login.text != "" &&
                          txtPassword.text != "") {
                        if (txtPassword.text.trim() ==
                            txtPasswordConfirmation.text.trim()) {
                          //Si c'est un user individuel
                          if (RecTypeUser == "I") {
                            print("Individuel");
                            Registration registration =  Registration(
                                'I',
                                inPays,
                                VarTel,
                                txtFullName.text.trim(),
                                login.text.trim(),
                                txtPassword.text.trim(),
                                RaisonSociale.text.trim(),
                                RCCM.text.trim());
                            registration.registration(context);
                          } else {
                            if (RaisonSociale.text.trim() != "" &&
                                RCCM.text.trim() != "") {
                              Registration registration = Registration(
                                  'B',
                                  inPays,
                                  VarTel,
                                  txtFullName.text.trim(),
                                  login.text.trim(),
                                  txtPassword.text.trim(),
                                  RaisonSociale.text.trim(),
                                  RCCM.text.trim());
                              registration.registration(context);
                              print("Entreprise");
                            }
                          }
                        } else {
                          ToastContext().init(context);
                          Composant().showToast(
                              "false", "Mot de passe de confirmation incorrect",
                              duration: Toast.lengthLong,
                              gravity: Toast.bottom);
                        }
                      } else {
                        ToastContext().init(context);
                        Composant().showToast(
                            "false", "Veuiller remplir vos champs de saisie",
                            duration: Toast.lengthLong, gravity: Toast.bottom);
                      }
                    },
                    child: Text(
                      "Créer",
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                  ),
                ),
              ],
            ),
          ),
          new Offstage(
            offstage: _loading,
            child: _progressWidget(),
          ),
        ],
      ),
    );
  }

  Widget _progressWidget() {
    return new Center(
      child: new Stack(
        children: <Widget>[
          new Container(
            color: Colors.black45,
          ),
          Center(
            child: new SpinKitSpinningLines(
              color: Colors.orangeAccent,
              size: 100,
              lineWidth: 5,
            ),
          )
        ],
      ),
    );
  }

  //Navigator.of(context)
  //  .push(MaterialPageRoute(builder: (context) => LoadingStyle()));

  void showProgress() {
    if (mounted) {
      setState(() {
        print(">>>>>> showProgress >>> _loading : ${_loading}");

        _loading = false;

        print(">>>>>> showProgress >>> _loading : ${_loading}");
      });
    }
  }

  void hideProgress() {
    if (mounted) {
      setState(() {
        print(">>>>>> hideProgress >>> ");
        _loading = true;
      });
    }
  }

  Widget VisibleEntreprise(Visibiliter) {
    return Visibility(
      child: Container(
        padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
        child: TextField(
          controller: RaisonSociale,
          decoration: InputDecoration(
            focusColor: Colors.orange,
            labelText: "Raison sociale",
            hintText: "Raison sociale",
          ),
          textInputAction: TextInputAction.done,
          keyboardType: TextInputType.text,
        ),
      ),
      visible: Visibiliter,
    );
  }

  Widget VisibleRCCM(Visibiliter) {
    return Visibility(
      child: Container(
        padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
        child: TextField(
          controller: RCCM,
          decoration: InputDecoration(
            focusColor: Colors.orange,
            labelText: "Registre de commerce",
            hintText: "Registre de commerce",
          ),
          textInputAction: TextInputAction.done,
          keyboardType: TextInputType.text,
        ),
      ),
      visible: Visibiliter,
    );
  }

  Widget ComboCkeck(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
      child: DropdownButtonFormField<String>(
        value: dropdownValue,
        elevation: 16,
        style: const TextStyle(color: Colors.black54, fontSize: 20),
        onChanged: (String? value) {
          setState(() {
            dropdownValue = value!;
          });
        },
        items: <String>['Individuel']
            .map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(
              value,
              style: TextStyle(fontSize: 18),
            ),
            onTap: () {
              if (value == 'Individuel') {
                setState(() {
                  RecTypeUser = "";
                  RecTypeUser = "I";
                  Visibiliter = false;
                });
              }
              print(value);
            },
          );
        }).toList(),
      ),
    );
  }

  void change() {
    IntlPhoneField(
        initialValue: "",
        onChanged: (phone) {
          VarTel = "";
          print(inPays);
        });
  }

  Widget phoneTF2(String label) => IntlPhoneField(
        controller: idCountry,
        disableLengthCheck: true,
        initialValue: "",
        onCountryChanged: (value) {
          return change();
        },
        decoration: InputDecoration(
          focusColor: Colors.orange,
          labelText: label,
        ),
        initialCountryCode: 'SN',
        invalidNumberMessage: "Numéro invalide",
        onChanged: (phone) {
          setState(() {
            VarTel = phone.number;
            inPays = phone.countryCode;
          });
        },
      );

  Widget jaugeCircular() {
    return SpinKitSquareCircle(
      color: Colors.white,
      size: 50,
      duration: Duration(milliseconds: 3000),
    );
  }

  Future<dynamic> registrationUserSimple(
      String name,
      String userLogin,
      String password,
      String phone,
      String nomUser,
      String countryCode,
      String motDePasse,
      String userType) async {
    String url = NolidoConstants.KEYCLOACK_URL;
    // 'https://www.qibisoft.com:8443/auth/realms/nolido/protocol/openid-connect/token';

    Map<String, String> headers = {
      'content-type': 'application/x-www-form-urlencoded',
      'cache-control': 'no-cache',
    };

    Map<String, String> data = {
      'username': userLogin,
      'password': password,
      'grant_type': 'password',
      'client_id': 'nolido-mobile-application'
    };

    Uri uri = Uri.parse(url);
    http.Response? response1 = null;
    try {
      http.Response response =
          await http.post(uri, headers: headers, body: data);
      // print(response.statusCode);

      if (response.statusCode == 200) {
        Map<String, dynamic> token = json.decode(response.body);
        String accessToken = token['access_token'];
        print("Access token " + accessToken);

        String url1 = NolidoConstants.SERVER_URL +
            NolidoConstants.REGISTRATION_URL +
            NolidoConstants.INFO_URL;
        //   'http://192.168.1.202:8080/v1/registration/phone';
        headers = {
          "Content-Type": "application/json",
          'cache-control': 'no-cache',
          'Accept': 'application/json',
          'Authorization': 'Bearer $accessToken'
        };

        try {
          Uri uri1 = Uri.parse(url1);
          response1 = await http.post(uri1,
              headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer $accessToken',
              },
              body: jsonEncode(<String, String>{
                "phone": phone,
                "login": nomUser,
                "countryCode": countryCode,
                "fullName": name,
                "password": motDePasse,
                "userType": userType
              }));

          if (response1.statusCode == 200) {
            return accessToken;
          }
        } catch (e) {
          Map<String, dynamic> error = json.decode(response1!.body);
          String errorMessage = error['message'];
          print(errorMessage);
          Composant().showToast("false", errorMessage,
              duration: Toast.lengthLong, gravity: Toast.bottom);
        }
      }
    } catch (e) {
      Map<String, dynamic> error = json.decode(response1!.body);
      String errorMessage = error['message'];
      print(errorMessage);
      Composant().showToast("false", errorMessage,
          duration: Toast.lengthLong, gravity: Toast.bottom);
    }
  }

//Entreprise
  Future<dynamic> registrationUserEntreprise(
      String name,
      String register,
      String socialReason,
      String userLogin,
      String password,
      String phone,
      String nomUser,
      String countryCode,
      String motDePasse,
      String userType) async {
    String url = NolidoConstants.KEYCLOACK_URL;
    // 'https://www.qibisoft.com:8443/auth/realms/nolido/protocol/openid-connect/token';

    Map<String, String> headers = {
      'content-type': 'application/x-www-form-urlencoded',
      'cache-control': 'no-cache',
    };

    Map<String, String> data = {
      'username': userLogin,
      'password': password,
      'grant_type': 'password',
      'client_id': 'nolido-mobile-application'
    };

    Uri uri = Uri.parse(url);
    http.Response response = await http.post(uri, headers: headers, body: data);
    // print(response.statusCode);

    if (response.statusCode == 200) {
      Map<String, dynamic> token = json.decode(response.body);
      String accessToken = token['access_token'];
      print("Access token " + accessToken);

      String url1 = NolidoConstants.SERVER_URL +
          NolidoConstants.REGISTRATION_URL +
          NolidoConstants.INFO_URL;
      //   'http://192.168.1.202:8080/v1/registration/phone';
      headers = {
        "Content-Type": "application/json",
        'cache-control': 'no-cache',
        'Accept': 'application/json',
        'Authorization': 'Bearer $accessToken'
      };

      Uri uri1 = Uri.parse(url1);
      http.Response response1 = await http
          .post(uri1,
              headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer $accessToken',
              },
              body: jsonEncode(<String, String>{
                "register": register,
                "socialReason": socialReason,
                "phone": phone,
                "login": nomUser,
                "countryCode": countryCode,
                "fullName": name,
                "password": motDePasse,
                "userType": userType
              }))
          .whenComplete(() => print("operation ok"));
      print(response1.statusCode);
      print(response1.body);

      if (response1.statusCode == 200) {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => Loading()));
        return token;
      } else {
        ToastContext().init(context);
        Composant().showToast("false", "Ce compte a deja été crée",
            duration: Toast.lengthLong, gravity: Toast.bottom);

        return throw Exception('Compte deja créer');
      }
    } else if (response.statusCode == 401) {
      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "login ou mot de passe incorrect",
          duration: Toast.lengthLong, gravity: Toast.bottom);
      return throw Exception('Connexion non autorisée');
    } else if (response.statusCode == 400) {
      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "Ce compte a deja été crée",
          duration: Toast.lengthLong, gravity: Toast.bottom);
      return throw Exception('La vérification par email est requise');
    } else {
      //Toast Message
      ToastContext().init(context);
      Composant().showToast("false", "Le serveur rencontre une erreur",
          duration: Toast.lengthLong, gravity: Toast.bottom);

      throw Exception('Le serveur rencontre une erreur');
    }
  }
}
