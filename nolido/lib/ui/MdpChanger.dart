import 'package:flutter/material.dart';
import 'package:nolido/widget/Composant.dart';
import 'package:toast/toast.dart';


class MdpChanger extends StatefulWidget {
  const MdpChanger({Key? key}) : super(key: key);

  @override
  _MdpChangerState createState() => _MdpChangerState();
}

class _MdpChangerState extends State<MdpChanger> {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Row(
              children: [
                Container(
                  padding: EdgeInsets.fromLTRB(0, 35, 0, 0),
                  child: IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: Icon(Icons.arrow_back_ios_outlined, color: Colors.black54,)),
                )
              ],
            ),
            Column(
              children: [
                Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.fromLTRB(20, 5, 0, 0),
                  child: Column(
                    children: [
                      Text(
                        "Changer",
                        style: TextStyle(fontSize: 25, color: Colors.black54,fontWeight: FontWeight.bold),
                      ),

                    ],
                  ),
                ),

                Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                  child: Column(
                    children: [
                      Text(
                        "Mot de passe",
                        style: TextStyle(fontSize: 25, color: Colors.black54,fontWeight: FontWeight.bold),
                      ),

                    ],
                  ),
                ),
              ],
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
              child: Column(
                children: [
                  SizedBox(height: 25,),
                  Text(
                    "Entrez votre ancien mot de passe puis veuillez changer et confirmé",
                    style: TextStyle(fontSize: 15, color: Colors.black54),
                  ),

                ],
              ),
            ),

            Container(
              padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
              child: TextField(
                keyboardType: TextInputType.text,
                obscureText: true,
                decoration: InputDecoration(
                  labelText: 'Ancien mot de passe',

                ),
              ),
            ),

            Container(
              padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
              child: TextField(
                keyboardType: TextInputType.text,
                obscureText: true,
                decoration: InputDecoration(
                  labelText: 'Nouveau mot de passe',

                ),
              ),
            ),

            Container(
              padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
              child: TextField(
                keyboardType: TextInputType.text,
                obscureText: true,
                decoration: InputDecoration(
                  labelText: 'Confirmer le mot de passe',

                ),
              ),
            ),

            Container(
              width: MediaQuery.of(context).size.width,
              height: 85,
              padding: EdgeInsets.fromLTRB(20, 30, 20, 10),
              child: ElevatedButton(
                onPressed: () {
                  ToastContext().init(context);
                  Composant().showToast("False","Veuiller entrez votre mot de passe !",duration: Toast.lengthLong,gravity: Toast.bottom);
                  //Toast.show("Toast plugin app", duration: Toast.lengthShort, gravity:  Toast.bottom);
                  //Navigator.of(context).push(
                     // MaterialPageRoute(builder: (context) => Add()));
                },
                child: Text(
                  "Enregistrer",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
            ),

          ],
        ),
      ),
    );
  }
}
