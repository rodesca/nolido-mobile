import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:nolido/ui/Connexion.dart';
import 'package:toast/toast.dart';

import 'Composant.dart';

class Loading extends StatefulWidget {
  const Loading({Key? key}) : super(key: key);

  @override
  _LoadingState createState() => _LoadingState();
}

class _LoadingState extends State<Loading> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Timer(Duration(seconds: 2), callback);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: loading(),
          ),
        ],
      ),
    );
  }

  callback() {
    //Toast Message
    ToastContext().init(context);
    Composant().showToast("true", "Votre compte nolido à été crée",
        duration: Toast.lengthLong, gravity: Toast.bottom);
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => Connexion()));
  }

  callConnexionErreur() {
    Navigator.pop(context);
    //Toast Message
    ToastContext().init(context);
    Composant().showToast("false", "Ce compte a deja été créer",
        duration: Toast.lengthLong, gravity: Toast.bottom);
  }

  Widget loading() {
    return const SpinKitSpinningLines(
      color: Colors.orangeAccent,
      size: 100,
      duration: Duration(seconds: 1),
      lineWidth: 5,
    );
  }
}
