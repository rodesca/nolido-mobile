import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:toast/toast.dart';

class Composant {
  /**
   * champ de texte
   */
  var VarTel;
  var VarMdp;

  Widget ModeFinance(String labelText) {
    return GestureDetector(
      onTap: () {},
      child: Container(
        child: TextField(
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            suffixIcon: Icon(Icons.arrow_drop_down_circle_outlined),
            enabled: false,
            focusColor: Colors.orange,
            labelText: labelText,
          ),
          textInputAction: TextInputAction.done,
          keyboardType: TextInputType.emailAddress,
        ),
      ),
    );
  }

  Widget inputField(String labelText) => Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          border: Border.all(color: Colors.white)),
      child: Padding(
        padding: const EdgeInsets.only(left: 6, right: 4),
        child: TextField(
          decoration: InputDecoration(
            focusColor: Colors.orange,
            hintText: labelText,
          ),
          textAlign: TextAlign.center,
        ),
      ));

  Widget PopEdit() {
    return Container(
      child: AlertDialog(
        title: const Text(
          "Mode de financement",
          style: TextStyle(fontSize: 15, color: Colors.orange),
        ),
        content: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: const [
            Icon(Icons.attach_money_rounded),
            Icon(Icons.attach_money_rounded),
            Icon(Icons.attach_money_rounded),
            Icon(Icons.attach_money_rounded),
          ],
        ),
        elevation: 30,
        // backgroundColor: Colors.orange,
        actions: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                onPressed: () {},
                child: Text("Valider"),
              ),
              ElevatedButton(
                onPressed: () {
                  //Navigator.pop(context);
                },
                child: Text("Annuler"),
              ),
            ],
          ),
        ],
      ),
    );
  }
  /**
   * titre du page
   */

  Widget titrePage(String label) => Text(
        label,
        style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold),
      );

  /**
   * adresse email
   */
  Widget emailTF(Object controler, String labelText) => TextField(
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          focusColor: Colors.orange,
          labelText: labelText,
          hintText: "Entrez votre " + labelText,
        ),
        textInputAction: TextInputAction.done,
        keyboardType: TextInputType.emailAddress,
      );

  /**
   * texte simple
   */
  Widget texteTF(Object controler, String labelText) => Container(
        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
        child: TextField(
          decoration: InputDecoration(
            focusColor: Colors.orange,
            labelText: labelText,
            hintText: "Entrez votre " + labelText,
          ),
          textInputAction: TextInputAction.done,
          keyboardType: TextInputType.text,
        ),
      );

  /**
   * texte simple
   */
  Widget texteTfReadOnly(Object controler, String labelText) => Container(
        padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
        child: TextField(
          readOnly: false,
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            focusColor: Colors.orange,
            labelText: labelText,
            hintText: "Entrez votre " + labelText,
          ),
          textInputAction: TextInputAction.done,
          keyboardType: TextInputType.text,
        ),
      );

  /**
   * texte simple
   */
  Widget numberTF(Object controler, String labelText) => TextField(
        decoration: InputDecoration(
          prefixText: "XOF    ",
          suffixIcon: Image(image: AssetImage("images/visa1.png"),width: 20,height: 20,fit: BoxFit.cover,),
          //  border: OutlineInputBorder(),
          focusColor: Colors.orange,
          labelText: labelText,
          hintText: "Entrez votre " + labelText,
        ),
        textInputAction: TextInputAction.done,
        keyboardType: TextInputType.number,
        inputFormatters: [CurrencyTextInputFormatter(symbol: '')],
      );



  /**
   * texte simple
   */
  Widget passwordTF(Object controler, String labelText) => TextField(
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          focusColor: Colors.orange,
          labelText: labelText,
          hintText: "Entrez votre " + labelText,
        ),
        textInputAction: TextInputAction.done,
        keyboardType: TextInputType.text,
        obscureText: true,
        onChanged: (text) {
          VarMdp = '$text';
          //print('First text field: $text');
        },
      );

  
  void change() {
    IntlPhoneField(
        initialValue: "",
        onChanged: (phone) {
          VarTel = "";
        });
  }

  Widget phoneTF2(String label) => IntlPhoneField(
        disableLengthCheck: true,
        initialValue: "",
        onCountryChanged: (value) {
          return change();
        },
        decoration: InputDecoration(
          focusColor: Colors.orange,
          labelText: label,
        ),
        initialCountryCode: 'SN',
        invalidNumberMessage: "Numéro invalide",
        onChanged: (phone) {
          VarTel = phone.completeNumber;
          print(VarTel);
        },
      );

  /**
   * texte simple
   */
  Widget phoneTF(Object controler, String labelText) => TextField(
        decoration: InputDecoration(
          focusColor: Colors.orange,
          border: OutlineInputBorder(),
          labelText: labelText,
          hintText: "Entrez votre " + labelText,
        ),
        textInputAction: TextInputAction.done,
        keyboardType: TextInputType.phone,
      );

  Widget itemTop(String label) => Container(
        padding: EdgeInsets.all(8),
        height: 50,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5),
              topRight: Radius.circular(5),
            )),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            TextButton(onPressed: () {}, child: Text(label)),
            Icon(Icons.arrow_right_sharp)
          ],
        ),
      );

  Widget itemBottom(String label) => Container(
        padding: EdgeInsets.all(8),
        height: 50,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(5),
              bottomRight: Radius.circular(5),
            )),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            TextButton(onPressed: () {}, child: Text(label)),
            Icon(Icons.arrow_right_sharp)
          ],
        ),
      );

  Widget itemMidle(String label) => Container(
        padding: EdgeInsets.all(8),
        height: 50,
        color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            TextButton(onPressed: () {}, child: Text(label)),
            Icon(Icons.arrow_right_sharp)
          ],
        ),
      );

  List<String> liste = ['Airtel Money', 'MTN Mobile Money', 'UBA Africa'];

  Widget dropDownMenu(List<String> list) => DropdownButton(
        hint: Text("Sélectionner"),
        dropdownColor: Colors.white,
        icon: Icon(Icons.arrow_drop_down),
        iconSize: 36,
        isExpanded: true,
        style: TextStyle(fontSize: 22, color: Colors.black),
        items: liste.map((item) {
          return DropdownMenuItem(
            value: item,
            child: Text(item),
          );
        }).toList(),
        onChanged: (String? value) {},
      );

  /**
   * menu deroulant
   */

  Widget dropDownMenuA(List list, String valueChoose) => Container(
      padding: EdgeInsets.only(left: 16, right: 16),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          border: Border.all(color: Colors.grey, width: 1)),
      child: DropdownButton(
        hint: Text("Votre choix"),
        icon: Icon(Icons.arrow_drop_down),
        iconSize: 36,
        isExpanded: true,
        underline: SizedBox(),
        style: TextStyle(color: Colors.black, fontSize: 22),
        value: valueChoose,
        items: list.map((_var) {
          return DropdownMenuItem(value: _var, child: Text(_var));
        }).toList(),
        onChanged: (Object? value) {},
      ));

  void showToast(String etat, String msg, {int? duration, int? gravity}) {
    Toast.show(msg,
        duration: duration,
        gravity: gravity,
        backgroundColor: etat =="true" ? Colors.green : Colors.redAccent);
  }

}
