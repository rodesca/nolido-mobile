import 'package:flutter/foundation.dart';
import 'package:sqflite/sqflite.dart' as sql;

class SQLHelper {
   Future<void> createTables(sql.Database database) async {
    await database.execute("""CREATE TABLE tusr(
        usrid TEXT PRIMARY KEY AUTOINCREMENT NOT NULL,
        accountusr TEXT,
        currencycd TEXT,
        indicative TEXT,
        fullnameusr TEXT,
        countryCd  TEXT
      )
      """);
  }
// id: the id of a item
// title, description: name and description of your activity
// created_at: the time that the item was created. It will be automatically handled by SQLite

   Future<sql.Database> db() async {
    return sql.openDatabase(
      'nolido.db',
      version: 1,
      onCreate: (sql.Database database, int version) async {
        await createTables(database);
      },
    );
  }

  // Createusr
   Future<int> createUsr(String usrId, String accountusr,String currencycd,String indicative, String fullnameusr, String countryCd) async {
    final db = await this.db();

    final data = {'usrId': usrId, 'accountusr': accountusr,'currencycd':currencycd,'indicative':indicative,'fullnameusr':fullnameusr,'countryCd':countryCd};
    final id = await db.insert('tusr', data,
        conflictAlgorithm: sql.ConflictAlgorithm.replace);
    return id;
  }


   Future<List<Map<String, dynamic>>> getUsrs() async {
    final db = await this.db();

    return db.query('tusr');
  }


}